@startuml
left to right direction
skinparam packageStyle rectangle
actor 1.customer
actor 2.hotel_admin
actor 3.restaurant_admin
rectangle TakeRestEatBest {
   
    1.customer --> (Search Room)
    1.customer --> (Search Restaurant)
    1.customer --> (check Availability)
    1.customer --> (Register / login)
    
    
    (Register / login) <-- 2.hotel_admin
    (Hotel bookings) <-- 2.hotel_admin
    (Register / login) <-- 3.restaurant_admin  
    (modify hotel details) <-- 2.hotel_admin 
    (restaurant bookings) <-- 3.restaurant_admin
    1.customer --> (Explore Restaurant)
    1.customer --> (Book Table)
    1.customer --> (Book room)
    (modify restaurant details) <-- 3.restaurant_admin

}
@enduml

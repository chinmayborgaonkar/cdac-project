const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')
const crypto = require('crypto-js')
const mailer = require('../../mailer')
const uuid = require('uuid')

// multer: used for uploading document
const multer = require('multer')
const { response } = require('express')
const upload = multer({ dest: 'images/' })

const router = express.Router()

//========================= GET =========================

//========================= Get All Rooms =========================
router.get('/', (request, response) => {

    const statement = `select * from hotelRoomDetails where hotelId = ${request.userId};`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get Rooms According To Categories =========================
router.post('/by-category', (request, response) => {
    const { category } = request.body

    // const statementPics = `SELECT R.hotelRoomId, R.hotelRoomCategory, P.hotelRoomPic FROM hotelRoomDetails R INNER JOIN hotelRoomPicDetails P 
    //                         ON R.hotelRoomId = P.hotelRoomId where R.hotelRoomCategory = '${category}'
    //                         AND R.hotelId = ${request.userId};`

    const statement = `select * from hotelRoomDetails where hotelRoomCategory = '${category}'
                            AND hotelId = ${request.userId};`


    db.query(statement, (error, data) => {
        let dbResult = {}
        dbResult.roomDetails = data
        const statementPics = `SELECT P.hotelRoomId, P.hotelRoomPic FROM hotelRoomDetails R INNER JOIN hotelRoomPicDetails P 
                                    ON R.hotelRoomId = P.hotelRoomId where R.hotelRoomCategory = '${category}'
                                    AND R.hotelId = ${request.userId};`


        db.query(statementPics, (error, dataPics) => {
            dbResult.roomPics = dataPics
            response.send(utils.createResult(error, dbResult))
                // response.send(utils.createResult(error, dataPics))
        })
    })
})


//========================= POST =========================

//========================= Add Room =========================
router.post('/', (request, response) => {
    const { hotelRoomPrice, hotelRoomCategory, hotelRoomAccomodation } = request.body

    const statement = `
        insert into hotelRoomDetails 
        (hotelId, hotelRoomPrice, hotelRoomCategory, hotelRoomAccomodation) 
        values 
        (${request.userId},'${hotelRoomPrice}','${hotelRoomCategory}','${hotelRoomAccomodation}')
        `
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

//========================= upload hotel-rooms picture =========================
router.post('/upload-pic/:hotelRoomId', upload.single('image'), (request, response) => {
    const fileName = request.file.filename
    const { hotelRoomId } = request.params

    const statement = `insert into hotelRoomPicDetails (hotelId, hotelRoomId, hotelRoomPic) 
            values(${request.userId},'${hotelRoomId}','${fileName}') `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= PUT =========================

//========================= Edit Room details =========================
router.put('/:hotelRoomId', (request, response) => {
    const { hotelRoomId } = request.params
    const { hotelRoomPrice, hotelRoomCategory, hotelRoomAccomodation } = request.body

    const statement = `
        update hotelRoomDetails set 
            hotelRoomPrice = '${hotelRoomPrice}',
            hotelRoomCategory = '${hotelRoomCategory}',
            hotelRoomAccomodation = '${hotelRoomAccomodation}'
        where hotelRoomId = ${hotelRoomId} AND hotelId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= DELETE =========================

//========================= Delete Room =========================
router.delete('/single-delete/:hotelRoomId', (request, response) => {
    const { hotelRoomId } = request.params

    const statement = `DELETE FROM hotelRoomPicDetails WHERE hotelRoomId = ${hotelRoomId}`

    db.query(statement, (error, data) => {

        const statement2 = ` DELETE FROM hotelRoomDetails WHERE hotelRoomId = ${hotelRoomId}; `

        db.query(statement2, (error, data) => {
            response.send(utils.createResult(error, data))

        })
    })
})

//========================= Delete All Room =========================
router.delete('/delete-all', (request, response) => {

    const statement = `DELETE FROM hotelRoomPicDetails WHERE hotelId = ${request.userId}`

    db.query(statement, (error, data) => {

        const statement2 = `DELETE FROM hotelRoomDetails WHERE hotelId = ${request.userId}`

        // console.log("userId :" + request.userId)
        db.query(statement2, (error, data) => {
            response.send(utils.createResult(error, data))
        })
    })
})

module.exports = router
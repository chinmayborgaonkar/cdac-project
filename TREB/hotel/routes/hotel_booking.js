const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

//========================= GET =========================

//========================= Get All Bookings =========================
router.get('/all', (request, response) => {

    const statement = `select * from custHotelBookingInfo WHERE hotelId = ${request.userId};`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get Details of customer by hotelBookingId =========================
router.get('/custBookingDetails/:hotelBookingId', (request, response) => {

    const { hotelBookingId } = request.params

    const statement = `select c.custFirstName, c.custLastName, c.custEmail, c.custMobileNo, c.custAddress, c.cityPin
                        from customerInfo c inner join custHotelBookingInfo h
                        on c.customerId = h.customerId
            where h.hotelBookingId = ${hotelBookingId} `


    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== Get details by BookingStatus =========================
router.get('/status', (request, response) => {
    const { bookingStatus } = request.body

    let statement = '';

    if (bookingStatus == "Completed" || bookingStatus == "Cancelled") {
        statement = `select * from custHotelBookingInfo where custBookingStatus='${bookingStatus}' AND hotelId = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    } else {
        statement = ` select * from custHotelBookingInfo
         where (custBookingStatus="Completed") 
        AND (( CURDATE() >= custCheckInDate ) AND (custCheckOutDate >= CURDATE() )) AND hotelId = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    }
})


//========================= Search Booking by Booking ID =========================
router.get('/search-by-id/:hotelId', (request, response) => {
    const { hotelId } = request.params

    const statement = `select * from custHotelBookingInfo WHERE  hotelId = ${hotelId};`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Search Booking by Date =========================
router.get('/search-by-date', (request, response) => {
    const { custCheckInDate } = request.body

    const statement = `select * from custHotelBookingInfo WHERE custCheckInDate = '${custCheckInDate}'
                     AND hotelId = ${request.userId};`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get All Ratings Given By Customer =========================
router.get('/display-ratings', (request, response) => {

    const statement = `
        select 
        b.customerId,
        c.custFirstName,
        c.custLastName,
        b.custHotelRating
        FROM custHotelBookingInfo b INNER JOIN customerInfo c
        ON b.customerId = c.customerId 
        AND hotelId = ${request.userId};    `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get Customer By Name =========================
router.get('/search-by-name', (request, response) => {
    const { custFirstName, custLastName } = request.body
    const statement = `
        SELECT 
        b.customerId,
        b.hotelId,
        b.custBookedHotel,
        b.custCheckInDate,
        b.custCheckOutDate,
        b.custNoOfGuests,
        b.custBookingStatus,
        b.custNoOfRooms,
        b.cityPin
        FROM custHotelBookingInfo b LEFT JOIN customerInfo c
        ON b.customerID = c.customerID
        WHERE custFirstName = '${custFirstName}' AND custLastName = '${custLastName}'
        AND hotelId = ${request.userId};    `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get All Guests for Specific Customer =========================
router.get('/display-guest-list/:hotelBookingId', (request, response) => {
    const { hotelBookingId } = request.params

    const statement = `SELECT * FROM custHotelGuestsList where hotelBookingId = ${hotelBookingId}`



    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Add Bookings =========================
router.post('/Add-Bookings', (request, response) => {
    const {
        customerId,
        hotelId,
        custBookedHotel,
        custCheckInDate,
        custCheckOutDate,
        custNoOfGuests,
        custBookingStatus,
        custNoOfRooms,
        cityPin
    } = request.body

    const statement = `insert into custHotelBookingInfo 
    (customerId, hotelId, custBookedHotel,custCheckInDate, custCheckOutDate, custNoOfGuests, custBookingStatus, custNoOfRooms, cityPin) 
    values 
    (   ${customerId},
        ${hotelId},
        '${custBookedHotel}',
        '${custCheckInDate}',
        '${custCheckOutDate}',
        ${custNoOfGuests},
        '${custBookingStatus}',
        ${custNoOfRooms},
        ${cityPin}
    )`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


module.exports = router
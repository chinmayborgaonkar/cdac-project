const express = require("express")
const db = require("../../db")
const utils = require("../../utils")
const router = express.Router()


//========================POST=========================

//========================Get All Hotels=========================
router.get('/All', (request, response) => {

    const statement = `select hotelId,hotelName,hotelAdminName,hotelMobileNo,hotelEmail,hotelNoOfRooms from hotelInfo`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================Get Hotel by Id=========================
router.post('/Id', (request, response) => {
    const { hotelId } = request.body

    const statement = `select * from hotelInfo where hotelId=${hotelId}`

    db.query(statement, (error, data) => {
        const dbResult = {
            hotelDetails: data
        }

        const statementPics = `select * from hotelPicDetails where  hotelId=${hotelId} `

        db.query(statementPics, (error, dataPics) => {
            dbResult.hotelPics = dataPics

            const dataRoom = `select room.hotelRoomId,room.hotelRoomPrice,room.hotelRoomCategory,room.hotelRoomAccomodation from hotelRoomDetails room inner join hotelInfo h ON room.hotelId=h.hotelId where h.hotelId=${hotelId}`

            db.query(dataRoom, (error, roomData) => {
                dbResult.Roomdata = roomData
                response.send(utils.createResult(error, dbResult))
            })
        })
    })
})

//========================Get Hotel by booking Id=========================
router.post('/BookingId', (request, response) => {
    const { hotelBookingId } = request.body

    const statement = `select h.hotelId, h.hotelName,h.hotelMobileNo,h.hotelEmail,h.hotelAddressLandmark,h.cityPin,ch.custNoOfGuests,ch.custNoOfRooms from hotelInfo h inner join custHotelBookingInfo ch
    ON h.hotelId = ch.hotelId
    where hotelBookingId=${hotelBookingId} AND customerId = ${request.userId}`

    db.query(statement, (error, data) => {

        const statementPics = `select * from hotelPicDetails where  hotelId=${data[0].hotelId} `

        db.query(statementPics, (error, dataPics) => {
            const dbResult = {
                hotelDetails: data,
                hotelPics: dataPics
            }
            response.send(utils.createResult(error, dbResult))
        })
    })
})

//========================Get Hotel by city=========================
router.post('/city', (request, response) => {
    const { city } = request.body

    const statement = `select * from hotelInfo h inner join pinInfo pin
    ON h.cityPin = pin.cityPin
    where pin.city='${city}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================Get Hotel by name=========================
router.post('/name', (request, response) => {
    const { hotelName } = request.body

    const statement = `select * from hotelInfo where hotelName='${hotelName}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data));
    })
})

//========================Get Hotel by price=========================
router.post('/price', (request, response) => {
    const { startRange, endRange } = request.body

    const statement = `select *
    from hotelInfo h inner join hotelRoomDetails r 
    ON h.hotelId=r.hotelId
    where r.hotelRoomPrice BETWEEN ${startRange} AND ${endRange}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data));
    })
})

//========================Get All Room for specific hotel=========================
router.post('/allRooms', (request, response) => {
    const { hotelId } = request.body

    const RoomStatement = `select hotelRoomId,hotelRoomPrice,hotelRoomCategory,hotelRoomAccomodation from hotelRoomDetails where hotelId=${hotelId}`

    db.query(RoomStatement, (error, data) => {
        response.send(utils.createResult(error, data));
    })
})

module.exports = router
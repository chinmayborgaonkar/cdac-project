const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const crypto = require('crypto-js')
const mailer = require('../../mailer')
const uuid = require('uuid')
const jwt = require("jsonwebtoken")
const fs = require('fs')
    // Multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: __dirname + '/../../images/' })

const router = express.Router()

//======================== GET =========================

//======================== GET Profile =========================
router.get('/get-profile', (request, response) => {

        const statement = `select custFirstName, custLastName, custAddress, custMobileNo, custEmail, cityPin, custPassword from customerInfo where customerId = ${request.userId}`

        db.query(statement, (error, data) => {
            response.send(utils.createResult(error, data))
        })
    })
    //======================Get other info================================
router.get('/get-other-details', (request, response) => {

    const statement = `select * from customerOtherDetails where customerId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//=======================Add other info===============================

//======================== Forgot Password =========================
router.get('/forgot-password/:email', (request, response) => {
    const { email } = request.params

    const statement = `select customerId , custFirstName , custLastName from customerInfo where custEmail = '${email}'`

    db.query(statement, (error, customers) => {

        if (error) {
            response.send(utils.createError(error))
        } else if (customers.length == 0) {
            response.send(utils.createError('customer does not exists'))
        } else {
            const customer = customers[0]
            const otp = utils.generateOTP()

            const body = `Dear customer, use the following otp to reset your account pasword . Your otp = ${otp}`

            mailer.sendEmail(email, 'Reset your password', body, (error, info) => {
                response.send(utils.createResult(error, {
                    otp: otp,
                    email: email
                }))
            })
        }
    })
})

//======================== Customer Activation =========================
router.get('/activate/:token', (request, response) => {
    const { token } = request.params

    const statement = `update customerInfo set active = 1, activationToken = '' where activationToken = '${token}'`

    db.query(statement, (error, data) => {
        const body = ` activation successful....!!!!!`
        response.send(body)
    })
})

//======================== POST =========================

//======================== Sign Up =========================
router.post('/signup', (request, response) => {
    const { custFirstName, custLastName, custAddress, custMobileNo, custEmail, cityPin, custPassword } = request.body

    const encryptedCustPassword = crypto.SHA256(custPassword)
    const activationToken = uuid.v4()
    const activationLink = `http://localhost:3000/customer/activate/${activationToken}`

    const body = `
    <h1>welcome ${custFirstName}</h1>
    <p>please activate your account by visiting the following link</p>
    <p><a href="${activationLink}">activate</a></p>
    <p>admin,</p>
    <p>TREB.com</p>    `

    const statement = `insert into customerInfo (custFirstName,custLastName,custAddress ,custMobileNo,custEmail,
    cityPin,custPassword,activationToken)
   values ('${custFirstName}','${custLastName}','${custAddress}','${custMobileNo}',
   '${custEmail}','${cityPin}','${encryptedCustPassword}', '${activationToken}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
        const otherDetails = `insert into customerOtherDetails(customerId) values(${data['insertId']})`

        db.query(otherDetails, (error, data1) => {
            mailer.sendEmail(custEmail, "welcome to TakeRestEatBest ", body, (error, info) => {
                response.send(utils.createResult(error, data1))
            })
        })
    })
})

//======================== Sign In =========================
router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const statement = `select customerId , custFirstName , custLastName , active from customerInfo where custEmail = '${email}' and custPassword = '${crypto.SHA256(password)}'`

    db.query(statement, (error, customers) => {

        if (error) {
            response.send(utils.createError(error))
        } else if (customers.length == 0) {
            response.send(utils.createError('customer does not exists'))
        } else {
            const customer = customers[0]
            if (customer['active'] == 1) {
                const token = jwt.sign({ id: customer['customerId'] }, config.secret)

                response.send(utils.createResult(error, {
                    firstName: customer['custFirstName'],
                    lastName: customer['custLastName'],
                    token: token,
                    role: "customer"
                }))
            } else {
                response.send(utils.createError('Dear Customer, Your account is not activated. Please activate your account for signing in to our application.'))
            }
        }
    })
})

//======================== Upload Profile Pic =========================

router.get('/image/:filename', (request, response) => {
    const { filename } = request.params
    console.log(__dirname);
    // /home/sunbeam/cdac project/project git repo/take-rest-eat-best-project/TREB/customer/routes

    const file = fs.readFileSync(__dirname + '/../../images/' + filename)
    response.send(file)
})

router.get('/get-other-info', (request, response) => {

    const statement = `select custProfileImage from customerOtherDetails where customerId = ${request.userId} limit 1`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.post('/upload-profile-pic/', upload.single('image'), (request, response) => {
    const fileName = request.file.filename

    const statement = `update customerOtherDetails set custProfileImage='${fileName}' where customerId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== Reset Password =========================
router.post('/reset-password', (request, response) => {
    const { oldPassword, newPassword } = request.body

    const statement = `select custPassword from customerInfo where custPassword= '${crypto.SHA256(oldPassword)}' AND customerId=${request.userId}`

    db.query(statement, (error, data) => {

        if (error) {
            response.send("password does not match")
        } else {
            const ChangePass = `update customerInfo set custPassword='${crypto.SHA256(newPassword)}' where customerId=${request.userId}`

            db.query(ChangePass, (error, dataPass) => {
                response.send(utils.createResult(error, dataPass))
            })
        }
    })
});

//======================== PUT =========================

//======================== Update Profile =========================
router.put('/update-profile', (request, response) => {
        const { custFirstName, custLastName, custAddress, custMobileNo, cityPin, custEmail } = request.body

        const statement = `
        update customerInfo set 
        custFirstName = '${custFirstName}',
        custLastName = '${custLastName}',
        custAddress = '${custAddress}',
        custMobileNo = '${custMobileNo}',
        cityPin = '${cityPin}',
        custEmail = '${custEmail}'
        where customerId = ${request.userId}`

        db.query(statement, (error, data) => {
            response.send(utils.createResult(error, data))
        })
    })
    //==========================update other info============================
router.put('/update-other-details', (request, response) => {
    const { custMaritalStatus, custAge, custBirthDate, custGender } = request.body

    const statement = `
        update customerOtherDetails set 
        custMaritalStatus = '${custMaritalStatus}',
        custAge = '${custAge}',
        custBirthDate = '${custBirthDate}',
        custGender = '${custGender}'
        where customerId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== DELETE =========================

//======================== Delete Profile =========================
router.post('/delete-profile', (request, response) => {

    const statement = `update customerInfo set active=0 where customerId = ${request.userId}`
    console.log(request.userId)
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
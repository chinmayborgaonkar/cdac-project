const express = require("express")
const db = require("../../db")
const utils = require("../../utils")
const router = express.Router()

//======================== GET =========================

//======================== Get details by BookingStatus =========================
router.get('/status', (request, response) => {
    const { bookingStatus } = request.body

    //to get today's date
    let today = 'CURDATE()'

    let statement = '';

    if (bookingStatus == "Completed" || bookingStatus == "Cancelled") {
        statement = `select * from custHotelBookingInfo where custBookingStatus='${bookingStatus}' AND customerId = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    } else {
        statement = ` select * from custHotelBookingInfo
         where (custBookingStatus="Completed") 
        AND (( CURDATE() >= custCheckInDate ) AND (custCheckOutDate >= CURDATE() )) AND customerId = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    }
})

//=========================GET BOOKING DETAILS==============================
router.get('/get-booking-details/:sub', (request, response) => {
    const { sub } = request.params

    const statement = `select * from custHotelBookingInfo where hotelBookingId ='${sub}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//=========================Get Booking Details By Customer Id=====================
router.get('/get-booking-details-by-id', (request, response) => {

    const statement = `select * from custHotelBookingInfo where customerId='${request.userId}' And custBookingStatus='Completed' `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================POST=========================

//========================Customer Booking Hotel=========================
router.post('/', (request, response) => {
    const { hotelId, custBookedHotel, custCheckInDate, custCheckOutDate, custNoOfGuests, custNoOfRooms, cityPin, guests } = request.body;

    const statement = `insert into custHotelBookingInfo ( customerId, hotelId, custBookedHotel, custCheckInDate, custCheckOutDate, custNoOfGuests, custBookingStatus, custNoOfRooms, cityPin) values ('${request.userId}','${hotelId}','${custBookedHotel}','${custCheckInDate}','${custCheckOutDate}','${custNoOfGuests}','Completed','${custNoOfRooms}','${cityPin}')`;

    db.query(statement, (error, data) => {
        const hotelBookingId = data['insertId']

        let statementGuestDetails = `insert into custHotelGuestsList (hotelBookingId,custHotelGuestsName,custHotelGuestsAge,custHotelGuestsGender) values `

        for (let index = 0; index < guests.length; index++) {
            const guest = guests[index];

            if (index > 0) {
                statementGuestDetails += ', '
            }
            statementGuestDetails += `(${hotelBookingId}, '${guest['custHotelGuestsName']}', '${guest['custHotelGuestsAge']}','${guest['custHotelGuestsGender']}')`
        }

        db.query(statementGuestDetails, (error, data) => {

            const data1 = {
                msg: 'guests added',
                bookingId: hotelBookingId
            }
            response.send(utils.createSuccess(data1))
        })
    })
})

//========================PUT=========================

//========================= Cancel Restaurant Booking by Booking Id=========================
router.get('/cancel-booking/:id', (request, response) => {
    const { id } = request.params

    const statement = `update custHotelBookingInfo set custBookingStatus="Cancelled"
                       where hotelBookingId=${id} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================Rating Hotel =========================
router.put('/update-rating', (request, response) => {
    const { rate, bookingId } = request.body;

    const statement = `update custHotelBookingInfo set custHotelRating=${rate} where (customerId=${request.userId} AND hotelBookingId=${bookingId})`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
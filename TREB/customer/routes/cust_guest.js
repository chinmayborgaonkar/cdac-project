const express = require('express');
const db = require('../../db');
const utils = require('../../utils');

const router = express.Router()

//========================= GET =========================

//========================= Get all Guests =========================
router.get("/", (request, response) => {

    const statement = `select customerId,custGuestId,custGuestName,custGuestAge,custGuestGender from customerGuestList where customerId=${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get("/:id", (request, response) => {
    const { id } = request.params
    const statement = `select custGuestId,custGuestName,custGuestAge,custGuestGender from customerGuestList where custGuestId=${id} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
//========================= POST =========================

//========================= Add multiple Guest =========================
router.post("/", (request, response) => {
    const { guests } = request.body

    let statement = `insert into customerGuestList (customerId,custGuestName,custGuestAge,custGuestGender) values     `

    for (let index = 0; index < guests.length; index++) {
        const guest = guests[index];

        if (index > 0) {
            statement += ','
        }
        statement += `(${request.userId},'${guest.name}',${guest.age},'${guest.gender}')`
    }

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//===================ADD SINGLE GUEST===========================================
router.post("/insert", (request, response) => {
    const { name, age, gender } = request.body

    let statement = `insert into customerGuestList (customerId,custGuestName,custGuestAge,custGuestGender) values 
                         (${request.userId},'${name}',${age},'${gender}')    `


    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= PUT =========================

//========================= Update Guest =========================
router.put("/update:id", (request, response) => {
    const { id } = request.params
    const { name, age, gender } = request.body

    const statement = `update customerGuestList set custGuestName='${name}', custGuestAge=${age}, custGuestGender='${gender}' where (customerId=${request.userId} and custGuestId =${id}) `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= DELETE =========================

//========================= Delete specific guest from user list =========================
router.delete("/id:id", (request, response) => {
    const { id } = request.params

    const statement = `delete from customerGuestList where (customerId=${request.userId} AND custGuestId =${id} )`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Delete all guest from user list =========================
router.delete("/all", (request, response) => {
    const statement = `delete from customerGuestList where (customerId=${request.userId})`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
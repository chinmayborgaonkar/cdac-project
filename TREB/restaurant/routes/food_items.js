const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

//========================= GET =========================

//========================= Get Food Items by ID for specific Restaurant =========================
router.get('/', (request, response) => {
    const restId = request.userId
    const { id } = request.body

    const statement = `select * from restMenuDetails where restaurantId = ${restId} and foodItemId = ${id}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get All Food Items for specific Restaurant =========================
router.get('/getAll', (request, response) => {
    const restId = request.userId

    const statement = `select * from restMenuDetails where  restaurantId = ${restId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= POST =========================
//========================= ADD SINGLE ITEM =========================

router.post("/add", (request, response) => {
    const { name, price } = request.body

    let statement = `insert into restMenuDetails (restaurantId,foodItemName,foodItemPrice) values 
                         (${request.userId},'${name}',${price})`


    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Add Food Items for specific Restaurant =========================
router.post('/', (request, response) => {
    const { items } = request.body
    const restId = request.userId

    let statementAddItems = `insert into restMenuDetails (restaurantId , foodItemName , foodItemPrice) values `

    for (let index = 0; index < items.length; index++) {
        const item = items[index];
        if (index > 0) {
            statementAddItems += ', '
        }
        statementAddItems += `(${restId}, '${item['foodItemName']}', ${item['foodItemPrice']})`
    }

    db.query(statementAddItems, (error, data) => {
        response.send(utils.createSuccess('items added successfully'))
    })
})

//========================= PUT =========================

//========================= Edit Food Item  =========================

router.put('/update-menu', (request, response) => {
    const { foodItemName, foodItemAvailable } = request.body

    const statement = `update restMenuDetails set foodItemAvailable=${foodItemAvailable} 
                     where( restaurantId=${request.userId} AND foodItemName="${foodItemName}")`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= DELETE =========================

//========================= Delete Food Item by ID =========================
router.delete('/:itemId', (request, response) => {
    const { itemId } = request.params
    const restId = request.userId

    const statement = `delete from restMenuDetails where restaurantId = '${restId}' and foodItemId = '${itemId}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const crypto = require('crypto-js')
const mailer = require('../../mailer')
const uuid = require('uuid')
const jwt = require('jsonwebtoken')
const fs = require('fs')

// Multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: __dirname + '/../../images/' })

const router = express.Router()

//========================= GET =========================

//========================= Get Profile =========================
router.get('/get-profile', (request, response) => {
    const statement = `select restName, restAdminName, restMobileNo, restEmail, restAddressArea, restAddressStreet, restAddressLandmark, cityPin, restDescription, restOpenTime, restCloseTime, restNoOfTables, restPassword from restaurantInfo where restaurantId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Restaurant Activation  =========================
router.get('/activate/:token', (request, response) => {
    const { token } = request.params

    const statement = `update restaurantInfo set active = 1, activationToken = '' where activationToken = '${token}'`

    db.query(statement, (error, data) => {
        const body = `activation successful....!!!!!   `
        response.send(body)
    })
})


//========================= POST =========================

//========================= Forgot Password =========================
router.post('/forgot-password', (request, response) => {
    const { email } = request.body

    const statement = `select restaurantId , restName , restAdminName from restaurantInfo where restEmail = '${email}'`

    db.query(statement, (error, restaurants) => {

        if (error) {
            response.send(utils.createError(error))
        } else if (restaurants.length == 0) {
            response.send(utils.createError('Restaurant does not exists'))
        } else {
            const restaurant = restaurants[0]
            const otp = utils.generateOTP()
            const body = `Dear Rest Admin, use the following otp to reset your account pasword . Your otp = ${otp}`

            mailer.sendEmail(email, 'Reset your password', body, (error, info) => {
                response.send(utils.createResult(error, {
                    otp: otp,
                    email: email
                }))
            })
        }
    })
})

//========================= Restaurant Sign Up =========================
router.post('/signup', (request, response) => {
    const { restName, restAdminName, restMobileNo, restEmail, restAddressArea, restAddressStreet, restAddressLandmark, cityPin, restDescription, restOpenTime, restCloseTime, restNoOfTables, restPassword } = request.body

    const encryptedRestPassword = crypto.SHA256(restPassword)
    const activationTokenRest = uuid.v4()
    const activationLink = `http://localhost:4000/restaurant/activate/${activationTokenRest}`

    const body = `
    <h1>welcome ${restAdminName}</h1>
    <p>please activate your account by visiting the following link</p>
    <p><a href="${activationLink}">activate</a></p>
    <p>admin,</p>
    <p>TREB.com</p>    `

    const statement = `insert into restaurantInfo (restName,restAdminName,restMobileNo ,restEmail,restAddressArea,restAddressStreet,restAddressLandmark,cityPin,restDescription,restOpenTime,restCloseTime,restNoOfTables,restPassword,activationToken)
values ('${restName}','${restAdminName}','${restMobileNo}','${restEmail}','${restAddressArea}','${restAddressStreet}','${restAddressLandmark}', '${cityPin}','${restDescription}','${restOpenTime}','${restCloseTime}','${restNoOfTables}','${encryptedRestPassword}','${activationTokenRest}')`

    db.query(statement, (error, data) => {
        const restId = data['insertId']

        const otherDetails = `insert into restPicDetails(restaurantId) values(${restId})`

        db.query(otherDetails, (error, data1) => {
            const stmt = `insert into restTableDetails (restaurantId , restTotalNoOfTables ) values (${restId}, ${restNoOfTables})`

            db.query(stmt, (error, data2) => {
                mailer.sendEmail(restEmail, "welcome to TakeRestEatBest ", body, (error, info) => {
                    response.send(utils.createResult(error, data2))
                })
            })
        })
    })
})





//========================= Sign Up with Menu =========================
router.post('/signup-Menu', (request, response) => {
    const { restName, restAdminName, restMobileNo, restEmail, restAddressArea, restAddressStreet, restAddressLandmark, cityPin, restDescription, restOpenTime, restCloseTime, restNoOfTables, restPassword, items } = request.body
    const encryptedRestPassword = crypto.SHA256(restPassword)
    const activationTokenRest = uuid.v4()
    const activationLink = `http://localhost:4000/restaurant/activate/${activationTokenRest}`

    const body = `
    <h1>welcome ${restAdminName}</h1>
    <p>please activate your account by visiting the following link</p>
    <p><a href="${activationLink}">activate</a></p>
    <p>admin,</p>
    <p>TREB.com</p>    `

    const statement = `insert into restaurantInfo (restName,restAdminName,restMobileNo ,restEmail,restAddressArea,restAddressStreet,
                restAddressLandmark,cityPin,restDescription,restOpenTime,restCloseTime,restNoOfTables,restPassword,activationToken)
                values ('${restName}','${restAdminName}','${restMobileNo}','${restEmail}','${restAddressArea}','${restAddressStreet}',
                '${restAddressLandmark}', '${cityPin}','${restDescription}','${restOpenTime}','${restCloseTime}','${restNoOfTables}',
                '${encryptedRestPassword}','${activationTokenRest}')`

    db.query(statement, (error, data) => {

        mailer.sendEmail(restEmail, "welcome to TakeRestEatBest ", body, (error, info) => {
            response.send(utils.createResult(error, data))
        })

        const restaurantId = data['insertId']

        let statementAddItems = `insert into restMenuDetails (restaurantId , foodItemName , foodItemPrice) values `

        for (let index = 0; index < items.length; index++) {
            const item = items[index];

            if (index > 0) {
                statementAddItems += ', '
            }

            statementAddItems += `(${restaurantId}, '${item['foodItemName']}', ${item['foodItemPrice']})`
        }

        db.query(statementAddItems, (error, data) => {
            response.send(utils.createSuccess('items added successfully'))
        })
    })
})

//========================= Restaurant Sign In =========================
router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const statement = `select restaurantId , restName , restAdminName, active from restaurantInfo where restEmail = '${email}' and restPassword = '${crypto.SHA256(password)}'`

    db.query(statement, (error, restaurants) => {

        if (error) {
            response.send(utils.createError(error))
        } else if (restaurants.length == 0) {
            response.send(utils.createError('Restaurant does not exists'))
        } else {
            const restaurant = restaurants[0]

            if (restaurant['active'] == 1) {
                const token = jwt.sign({ id: restaurant['restaurantId'] }, config.secret)

                response.send(utils.createResult(error, {
                    restName: restaurant['restName'],
                    restAdminName: restaurant['restAdminName'],
                    token: token,
                    role: "restaurant"
                }))
            } else {
                response.send(utils.createError('Dear Restaurant Admin , Your account is not activated.Please activate your account for signing in our application'))
            }
        }
    })
})

//========================= Upload Restaurant Images =========================

router.get('/image/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../../images/' + filename)
    response.send(file)
})

router.get('/get-other-info', (request, response) => {

    const statement = `select restPic from restPicDetails where restaurantId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


router.post('/upload-rest-pics', upload.single('image'), (request, response) => {
    const fileName = request.file.filename

    const statement = `update restPicDetails set restPic='${fileName}' where restaurantId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== Reset Password =========================
router.post('/reset-password', (request, response) => {
    const { oldPassword, newPassword } = request.body

    const statement = `select restPassword from restaurantInfo where restPassword= '${crypto.SHA256(oldPassword)}' AND restaurantId=${request.userId} `

    db.query(statement, (error, data) => {

        if (error) {
            response.send("password does not match")
        } else {
            const ChangePass = `update restaurantInfo set restPassword='${crypto.SHA256(newPassword)}' where restaurantId=${request.userId}`

            db.query(ChangePass, (error, dataPass) => {
                response.send(utils.createResult(error, dataPass))
            })
        }
    })
});


//========================= PUT =========================

//========================= Update Profile =========================
router.put('/update-profile', (request, response) => {
    const { id } = request.params
    const { restName, restAdminName, restMobileNo, restEmail, restAddressArea, restAddressStreet, restAddressLandmark, cityPin, restDescription, restOpenTime, restCloseTime, restNoOfTables, restPassword } = request.body

    const statement = `update restaurantInfo set 
                        restName = '${restName}',
                        restAdminName = '${restAdminName}',
                        restMobileNo = '${restMobileNo}',
                        restEmail = '${restEmail}',
                        restAddressArea = '${restAddressArea}',
                        restAddressStreet = '${restAddressStreet}',
                        restAddressLandmark = '${restAddressLandmark}',
                        cityPin = '${cityPin}',
                        restDescription = '${restDescription}',
                        restOpenTime = '${restOpenTime}',
                        restCloseTime = '${restCloseTime}',
                        restNoOfTables = '${restNoOfTables}'
                       
                        where restaurantId = ${request.userId} `
        //  restPassword = '${restPassword}'
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= DELETE =========================

//========================= Delete Profile =========================
router.post('/delete-profile', (request, response) => {

    const statement = `update restaurantInfo set active = 0 where restaurantId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
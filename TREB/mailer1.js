const nodemailer = require('nodemailer')
const config = require('./config')

// Send Mail
function sendEmail(Email, subject, body, callback) {
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.emailId,
            pass: config.emailPassword
        }
    })

    const options = {
        from: config.emailId,
        to: Email,
        subject: subject,
        html: body
    }

    transport.sendMail(options, callback)
}
sendEmail("chinmaysb7@gmail.com", "check", "body", (error)=>{console.log(error)})
module.exports = {
    sendEmail: sendEmail
}

create database TREB;
 
use TREB; 

  create table pinInfo (
    cityPin integer PRIMARY KEY,
    city varchar(50)
  );


-- CUSTOMER TABLES  //3

  create table customerInfo (
    customerId integer primary key auto_increment,
    custFirstName varchar(100),
    custLastName varchar(100),
    custAddress varchar(100),
    custMobileNo varchar(15),
    custEmail varchar(50) UNIQUE,
    cityPin integer,
    custPassword varchar(100),
    custLoggedInStatus integer  DEFAULT 0,
    active Integer Default 0,
    activationToken VARCHAR(100),
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    foreign key (cityPin) references pinInfo(cityPin)
  );


 create table customerGuestList (
    customerId integer,
    custGuestId integer primary key auto_increment,
    custGuestName varchar(50),
    custGuestAge integer,
    custGuestGender varchar(15),
    foreign key (customerId) references customerInfo(customerId)
 );

 create table customerOtherDetails (
    customerId integer,
    custMaritalStatus varchar(50),
    custAge integer,
    custBirthDate date,
    custGender varchar(20),
    custProfileImage varchar(100),
    foreign key (customerId) references customerInfo(customerId)
 );


-- HOTEL TABLES //8

create table hotelInfo (
    hotelId integer primary key auto_increment,
    hotelName varchar(50),
    hotelAdminName varchar(50),
    hotelMobileNo varchar(15),
    hotelEmail varchar(50) UNIQUE,
    hotelAddressArea varchar(50),
    hotelAddressStreet varchar(50),
    hotelAddressLandmark varchar(50),
    cityPin integer,
    hotelDescription varchar(300),
    hotelNoOfRooms integer,
    hotelPassword varchar(100),
    hotelLoggedInStatus integer   DEFAULT 0,
    active Integer Default 0,
    activationToken VARCHAR(100),
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    foreign key (cityPin) references pinInfo(cityPin)
);

create table hotelPicDetails (
    hotelId integer,
    hotelPic varchar(100),
    foreign key (hotelId) references hotelInfo(hotelId)
);

create table hotelRoomDetails (
    hotelId integer,
    hotelRoomId integer primary key auto_increment,
    hotelRoomPrice double precision,
    hotelRoomCategory varchar(30),
    hotelRoomAccomodation integer,
    foreign key (hotelId) references hotelInfo(hotelId)
);

create table hotelRoomPicDetails (
    hotelId integer,
    hotelRoomId integer,
    hotelRoomPic varchar(100),
    foreign key (hotelId) references hotelInfo(hotelId),
    foreign key (hotelRoomId) references hotelRoomDetails(hotelRoomId)
);

create table hotelServiceDetails (
    hotelId integer,
    hotelServiceId integer primary key auto_increment,
    hotelServiceName varchar(50),
    hotelServiceAvailable integer,
    foreign key (hotelId) references hotelInfo(hotelId)
);
 
create table custHotelBookingInfo (
    hotelBookingId integer primary key auto_increment,
    customerId integer,
    hotelId integer,
    custBookedHotel varchar(50),
    custCheckInDate date,
    custCheckOutDate date,
    custNoOfGuests integer,
    custBookingStatus varchar(10),
    custHotelBookedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    custHotelRating Integer,
    custNoOfRooms integer,
    cityPin integer,
    foreign key (cityPin) references pinInfo(cityPin),
    foreign key (customerId) references customerInfo(customerId),
    foreign key (hotelId) references hotelInfo(hotelId)
);

create table custHotelGuestsList ( 
    hotelBookingId integer,
    custHotelGuestsName varchar(50),
    custHotelGuestsAge integer,
    custHotelGuestsGender varchar(15),
    foreign key (hotelBookingId) references custHotelBookingInfo(hotelBookingId)
);

create table custHotelRoomDetails (
    hotelBookingId integer,
    hotelRoomId integer,
    foreign key (hotelRoomId) references hotelRoomDetails(hotelRoomId),
    foreign key (hotelBookingId) references custHotelBookingInfo(hotelBookingId)
);


-- RESTAURANT TABLES //6

create table restaurantInfo (
    restaurantId integer primary key auto_increment,
    restName varchar(50),
    restAdminName varchar(50),
    restMobileNo varchar(15),
    restEmail varchar(50) UNIQUE,
    restAddressArea varchar(50),
    restAddressStreet varchar(50),
    restAddressLandmark varchar(50),
    cityPin integer,
    restDescription varchar(300),
    restOpenTime time,
    restCloseTime time,
    restNoOfTables integer,
    restPassword varchar(100),
    restLoggedInStatus integer   DEFAULT 0,
    active Integer Default 0,
    activationToken VARCHAR(100),
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    foreign key (cityPin) references pinInfo(cityPin)
);

create table restPicDetails (
    restaurantId integer,
    restPic varchar(100),
    foreign key (restaurantId) references restaurantInfo(restaurantId)
);

create table restMenuDetails (
    restaurantId integer,
    foodItemId integer primary key auto_increment,
    foodItemName varchar(50),
    foodItemPrice double precision,
    foodItemAvailable integer,
    foreign key (restaurantId) references restaurantInfo(restaurantId)
);

create table restTableDetails (
    restaurantId integer,
    restNoOfTableReserved integer,
    restTotalNoOfTables integer,
    foreign key (restaurantId) references restaurantInfo(restaurantId)
);

create table custRestBookingInfo (
    restBookingId integer primary key auto_increment,
    customerId integer,
    restaurantId integer,
    custBookedRest varchar(50),
    custRestBookingDate date,
    custRestBookedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    custRestRating integer,
    custBookingStatus varchar(10),
    custNoOfTables integer,
    cityPin integer,
    foreign key (cityPin) references pinInfo(cityPin),
    foreign key (customerId) references customerInfo(customerId),
    foreign key (restaurantId) references restaurantInfo(restaurantId)
);

create table custRestGuestsList ( 
    restBookingId integer,
    custRestGuestsName varchar(50),
    custRestGuestsAge integer,
    custRestGuestsGender varchar(15),
    foreign key (restBookingId) references custRestBookingInfo(restBookingId)
);
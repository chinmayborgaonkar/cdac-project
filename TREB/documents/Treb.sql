-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: localhost    Database: TREB
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `custHotelBookingInfo`
--

DROP TABLE IF EXISTS `custHotelBookingInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custHotelBookingInfo` (
  `hotelBookingId` int NOT NULL AUTO_INCREMENT,
  `customerId` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `custBookedHotel` varchar(50) DEFAULT NULL,
  `custCheckInDate` date DEFAULT NULL,
  `custCheckOutDate` date DEFAULT NULL,
  `custNoOfGuests` int DEFAULT NULL,
  `custBookingStatus` varchar(10) DEFAULT NULL,
  `custHotelBookedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `custHotelRating` int DEFAULT NULL,
  `custNoOfRooms` int DEFAULT NULL,
  `cityPin` int DEFAULT NULL,
  PRIMARY KEY (`hotelBookingId`),
  KEY `cityPin` (`cityPin`),
  KEY `customerId` (`customerId`),
  KEY `hotelId` (`hotelId`),
  CONSTRAINT `custHotelBookingInfo_ibfk_1` FOREIGN KEY (`cityPin`) REFERENCES `pinInfo` (`cityPin`),
  CONSTRAINT `custHotelBookingInfo_ibfk_2` FOREIGN KEY (`customerId`) REFERENCES `customerInfo` (`customerId`),
  CONSTRAINT `custHotelBookingInfo_ibfk_3` FOREIGN KEY (`hotelId`) REFERENCES `hotelInfo` (`hotelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custHotelBookingInfo`
--

LOCK TABLES `custHotelBookingInfo` WRITE;
/*!40000 ALTER TABLE `custHotelBookingInfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `custHotelBookingInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custHotelGuestsList`
--

DROP TABLE IF EXISTS `custHotelGuestsList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custHotelGuestsList` (
  `hotelBookingId` int DEFAULT NULL,
  `custHotelGuestsName` varchar(50) DEFAULT NULL,
  `custHotelGuestsAge` int DEFAULT NULL,
  `custHotelGuestsGender` varchar(15) DEFAULT NULL,
  KEY `hotelBookingId` (`hotelBookingId`),
  CONSTRAINT `custHotelGuestsList_ibfk_1` FOREIGN KEY (`hotelBookingId`) REFERENCES `custHotelBookingInfo` (`hotelBookingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custHotelGuestsList`
--

LOCK TABLES `custHotelGuestsList` WRITE;
/*!40000 ALTER TABLE `custHotelGuestsList` DISABLE KEYS */;
/*!40000 ALTER TABLE `custHotelGuestsList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custHotelRoomDetails`
--

DROP TABLE IF EXISTS `custHotelRoomDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custHotelRoomDetails` (
  `hotelBookingId` int DEFAULT NULL,
  `hotelRoomId` int DEFAULT NULL,
  KEY `hotelRoomId` (`hotelRoomId`),
  KEY `hotelBookingId` (`hotelBookingId`),
  CONSTRAINT `custHotelRoomDetails_ibfk_1` FOREIGN KEY (`hotelRoomId`) REFERENCES `hotelRoomDetails` (`hotelRoomId`),
  CONSTRAINT `custHotelRoomDetails_ibfk_2` FOREIGN KEY (`hotelBookingId`) REFERENCES `custHotelBookingInfo` (`hotelBookingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custHotelRoomDetails`
--

LOCK TABLES `custHotelRoomDetails` WRITE;
/*!40000 ALTER TABLE `custHotelRoomDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `custHotelRoomDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custRestBookingInfo`
--

DROP TABLE IF EXISTS `custRestBookingInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custRestBookingInfo` (
  `restBookingId` int NOT NULL AUTO_INCREMENT,
  `customerId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `custBookedRest` varchar(50) DEFAULT NULL,
  `custRestBookingDate` date DEFAULT NULL,
  `custRestBookedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `custRestRating` int DEFAULT NULL,
  `custBookingStatus` varchar(10) DEFAULT NULL,
  `custNoOfTables` int DEFAULT NULL,
  `cityPin` int DEFAULT NULL,
  PRIMARY KEY (`restBookingId`),
  KEY `cityPin` (`cityPin`),
  KEY `customerId` (`customerId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `custRestBookingInfo_ibfk_1` FOREIGN KEY (`cityPin`) REFERENCES `pinInfo` (`cityPin`),
  CONSTRAINT `custRestBookingInfo_ibfk_2` FOREIGN KEY (`customerId`) REFERENCES `customerInfo` (`customerId`),
  CONSTRAINT `custRestBookingInfo_ibfk_3` FOREIGN KEY (`restaurantId`) REFERENCES `restaurantInfo` (`restaurantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custRestBookingInfo`
--

LOCK TABLES `custRestBookingInfo` WRITE;
/*!40000 ALTER TABLE `custRestBookingInfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `custRestBookingInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custRestGuestsList`
--

DROP TABLE IF EXISTS `custRestGuestsList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custRestGuestsList` (
  `restBookingId` int DEFAULT NULL,
  `custRestGuestsName` varchar(50) DEFAULT NULL,
  `custRestGuestsAge` int DEFAULT NULL,
  `custRestGuestsGender` varchar(15) DEFAULT NULL,
  KEY `restBookingId` (`restBookingId`),
  CONSTRAINT `custRestGuestsList_ibfk_1` FOREIGN KEY (`restBookingId`) REFERENCES `custRestBookingInfo` (`restBookingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custRestGuestsList`
--

LOCK TABLES `custRestGuestsList` WRITE;
/*!40000 ALTER TABLE `custRestGuestsList` DISABLE KEYS */;
/*!40000 ALTER TABLE `custRestGuestsList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerGuestList`
--

DROP TABLE IF EXISTS `customerGuestList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customerGuestList` (
  `customerId` int DEFAULT NULL,
  `custGuestId` int NOT NULL AUTO_INCREMENT,
  `custGuestName` varchar(50) DEFAULT NULL,
  `custGuestAge` int DEFAULT NULL,
  `custGuestGender` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`custGuestId`),
  KEY `customerId` (`customerId`),
  CONSTRAINT `customerGuestList_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customerInfo` (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerGuestList`
--

LOCK TABLES `customerGuestList` WRITE;
/*!40000 ALTER TABLE `customerGuestList` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerGuestList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerInfo`
--

DROP TABLE IF EXISTS `customerInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customerInfo` (
  `customerId` int NOT NULL AUTO_INCREMENT,
  `custFirstName` varchar(100) DEFAULT NULL,
  `custLastName` varchar(100) DEFAULT NULL,
  `custAddress` varchar(100) DEFAULT NULL,
  `custMobileNo` varchar(15) DEFAULT NULL,
  `custEmail` varchar(50) DEFAULT NULL,
  `cityPin` int DEFAULT NULL,
  `custPassword` varchar(100) DEFAULT NULL,
  `custLoggedInStatus` int DEFAULT '0',
  `active` int DEFAULT '0',
  `activationToken` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customerId`),
  UNIQUE KEY `custEmail` (`custEmail`),
  KEY `cityPin` (`cityPin`),
  CONSTRAINT `customerInfo_ibfk_1` FOREIGN KEY (`cityPin`) REFERENCES `pinInfo` (`cityPin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerInfo`
--

LOCK TABLES `customerInfo` WRITE;
/*!40000 ALTER TABLE `customerInfo` DISABLE KEYS */;
INSERT INTO `customerInfo` VALUES (1,'Chinmay','Borgaonkar','Pune','7776854477','borgaonkarchinmays@gmail.com',431007,'424b805d7cc050a2d67b39adba9f0eb5e7a4f7c9825b522d17d9bfce518a5f4c',0,1,'','2021-01-17 16:57:53'),(2,'Vartika','Modi','Bhilai','8982365479','vartikamodi97@gmail.com',490020,'e9f88a985355b5ee6465d6f2315ce1ea246efeee8111a68553f4e0ebf0a2ac11',0,1,'','2021-01-17 17:06:59'),(3,'Vrushali','Jagtap','Baramati','9130821163','vrushali.jagtap199@gmail.com',413102,'dbaff13f73dbf5ea51a3c9739a38a2bee1c59400cbf47db8bf817004c2ccfd7b',0,1,'','2021-01-17 17:08:07'),(4,'Rasika','Satpute','Mumbai','8356077022','rasikasatpute5@gmail.com',400001,'44f9692a53ca9834ee2c1e8298b387bfc57e95401193ba35b200c4dba6b876e8',0,1,'','2021-01-17 17:15:44');
/*!40000 ALTER TABLE `customerInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerOtherDetails`
--

DROP TABLE IF EXISTS `customerOtherDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customerOtherDetails` (
  `customerId` int DEFAULT NULL,
  `custMaritalStatus` varchar(50) DEFAULT NULL,
  `custAge` int DEFAULT NULL,
  `custBirthDate` date DEFAULT NULL,
  `custGender` varchar(20) DEFAULT NULL,
  `custProfileImage` varchar(100) DEFAULT NULL,
  KEY `customerId` (`customerId`),
  CONSTRAINT `customerOtherDetails_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customerInfo` (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerOtherDetails`
--

LOCK TABLES `customerOtherDetails` WRITE;
/*!40000 ALTER TABLE `customerOtherDetails` DISABLE KEYS */;
INSERT INTO `customerOtherDetails` VALUES (1,'Single',25,'1995-10-12','Male','ac5e02731867d9f4ce5cff77798105e9'),(2,'Single',24,'1997-04-11','Female','b2635df9d65cc7b9b7a93b1947f34b3b'),(3,'Single',24,'1997-12-27','Female','7464958b471ba46646b0fa806879aa9f'),(4,'Single',26,'1994-12-08','Female','7fb0e12b5b93f94b1da34b506e47db79');
/*!40000 ALTER TABLE `customerOtherDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelInfo`
--

DROP TABLE IF EXISTS `hotelInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotelInfo` (
  `hotelId` int NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(50) DEFAULT NULL,
  `hotelAdminName` varchar(50) DEFAULT NULL,
  `hotelMobileNo` varchar(15) DEFAULT NULL,
  `hotelEmail` varchar(50) DEFAULT NULL,
  `hotelAddressArea` varchar(50) DEFAULT NULL,
  `hotelAddressStreet` varchar(50) DEFAULT NULL,
  `hotelAddressLandmark` varchar(50) DEFAULT NULL,
  `cityPin` int DEFAULT NULL,
  `hotelDescription` varchar(300) DEFAULT NULL,
  `hotelNoOfRooms` int DEFAULT NULL,
  `hotelPassword` varchar(100) DEFAULT NULL,
  `hotelLoggedInStatus` int DEFAULT '0',
  `active` int DEFAULT '0',
  `activationToken` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`hotelId`),
  UNIQUE KEY `hotelEmail` (`hotelEmail`),
  KEY `cityPin` (`cityPin`),
  CONSTRAINT `hotelInfo_ibfk_1` FOREIGN KEY (`cityPin`) REFERENCES `pinInfo` (`cityPin`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelInfo`
--

LOCK TABLES `hotelInfo` WRITE;
/*!40000 ALTER TABLE `hotelInfo` DISABLE KEYS */;
INSERT INTO `hotelInfo` VALUES (1,'Hotel Chinmay','Chinmay borgaonkar','7776854477','borgaonkarchinmays@gmail.com','FC road','Baner road','Baner road',431007,'This is nice hotel with beautiful ambience.',10,'37066099aa18a20e979a42b9e93fb9c3800a354d7efd755def065c741f8c5ea3',0,1,'','2021-01-17 17:26:31'),(2,'Hotel Rasika','Rasika Satpute','8356077022','rasikasatpute5@gmail.com','Cummins road ','karve nagar road','karve nagar road',431007,'This is nice hotel with beautiful ambience.',15,'5a1d4f97e2566a6711a68404d6ae2e488f1fd2aa159b07a09df5fb55152117f0',0,1,'','2021-01-17 17:29:34'),(4,'Hotel Vrushali','Vrushali Jagtap','9130821163','vrushali.jagtap199@gmail.com','Wakad ','Pimple Saudagar','Pimple Saudagar',431007,'This is nice hotel with beautiful ambience.',20,'98c8518eef5942e1bbd3735b4cab5dce4010fd12791bb5cd21ffda3681b8832e',0,1,'','2021-01-17 17:38:24'),(5,'Hotel Vartika','Vartika Modi','8982365479','vartikamodi97@gmail.com','Infosys phase 1 road ','Hinjewadi road','Hinjewadi road',431007,'This is nice hotel with beautiful ambience.',12,'3d8697ab24e327459998756e109fc51e643df4f09918ec6d3bd773e534fea6be',0,1,'','2021-01-17 17:42:29');
/*!40000 ALTER TABLE `hotelInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelPicDetails`
--

DROP TABLE IF EXISTS `hotelPicDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotelPicDetails` (
  `hotelId` int DEFAULT NULL,
  `hotelPic` varchar(100) DEFAULT NULL,
  KEY `hotelId` (`hotelId`),
  CONSTRAINT `hotelPicDetails_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotelInfo` (`hotelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelPicDetails`
--

LOCK TABLES `hotelPicDetails` WRITE;
/*!40000 ALTER TABLE `hotelPicDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `hotelPicDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelRoomDetails`
--

DROP TABLE IF EXISTS `hotelRoomDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotelRoomDetails` (
  `hotelId` int DEFAULT NULL,
  `hotelRoomId` int NOT NULL AUTO_INCREMENT,
  `hotelRoomPrice` double DEFAULT NULL,
  `hotelRoomCategory` varchar(30) DEFAULT NULL,
  `hotelRoomAccomodation` int DEFAULT NULL,
  PRIMARY KEY (`hotelRoomId`),
  KEY `hotelId` (`hotelId`),
  CONSTRAINT `hotelRoomDetails_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotelInfo` (`hotelId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelRoomDetails`
--

LOCK TABLES `hotelRoomDetails` WRITE;
/*!40000 ALTER TABLE `hotelRoomDetails` DISABLE KEYS */;
INSERT INTO `hotelRoomDetails` VALUES (1,1,2500,'Deluxe',2),(1,2,3500,'Super Deluxe',2),(1,3,5500,'Executive',4),(2,4,2000,'Deluxe',2),(2,5,3500,'Executive',3),(2,6,3500,'Super Deluxe',2),(5,7,2500,'Deluxe',2),(5,8,3500,'Super Deluxe',2),(5,9,5500,'Executive',4),(4,10,2000,'Deluxe',2),(4,11,3500,'Super Deluxe',2),(4,12,5000,'Maharaja Suite',2);
/*!40000 ALTER TABLE `hotelRoomDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelRoomPicDetails`
--

DROP TABLE IF EXISTS `hotelRoomPicDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotelRoomPicDetails` (
  `hotelId` int DEFAULT NULL,
  `hotelRoomId` int DEFAULT NULL,
  `hotelRoomPic` varchar(100) DEFAULT NULL,
  KEY `hotelId` (`hotelId`),
  KEY `hotelRoomId` (`hotelRoomId`),
  CONSTRAINT `hotelRoomPicDetails_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotelInfo` (`hotelId`),
  CONSTRAINT `hotelRoomPicDetails_ibfk_2` FOREIGN KEY (`hotelRoomId`) REFERENCES `hotelRoomDetails` (`hotelRoomId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelRoomPicDetails`
--

LOCK TABLES `hotelRoomPicDetails` WRITE;
/*!40000 ALTER TABLE `hotelRoomPicDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `hotelRoomPicDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelServiceDetails`
--

DROP TABLE IF EXISTS `hotelServiceDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotelServiceDetails` (
  `hotelId` int DEFAULT NULL,
  `hotelServiceId` int NOT NULL AUTO_INCREMENT,
  `hotelServiceName` varchar(50) DEFAULT NULL,
  `hotelServiceAvailable` int DEFAULT NULL,
  PRIMARY KEY (`hotelServiceId`),
  KEY `hotelId` (`hotelId`),
  CONSTRAINT `hotelServiceDetails_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotelInfo` (`hotelId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelServiceDetails`
--

LOCK TABLES `hotelServiceDetails` WRITE;
/*!40000 ALTER TABLE `hotelServiceDetails` DISABLE KEYS */;
INSERT INTO `hotelServiceDetails` VALUES (1,1,'Wifi',1),(1,2,'Parking',1),(1,3,'Swimming Pool',1),(1,4,'PickUp Drop',NULL),(2,5,'Wifi',1),(2,6,'Parking',NULL),(2,7,'Swimming Pool',1),(5,8,'AC',NULL),(5,9,'SPA',NULL),(5,10,'Wifi',NULL),(5,11,'Dry Cleaning',NULL),(5,12,'Parking',NULL),(4,13,'AC',1),(4,14,'SPA',NULL),(4,15,'Wifi',1),(4,16,'Parking',NULL),(4,17,'Swimming Pool',NULL);
/*!40000 ALTER TABLE `hotelServiceDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pinInfo`
--

DROP TABLE IF EXISTS `pinInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pinInfo` (
  `cityPin` int NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cityPin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pinInfo`
--

LOCK TABLES `pinInfo` WRITE;
/*!40000 ALTER TABLE `pinInfo` DISABLE KEYS */;
INSERT INTO `pinInfo` VALUES (400001,'Mumbai'),(413102,'Baramati'),(431007,'Pune'),(490020,'Bhilai');
/*!40000 ALTER TABLE `pinInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restMenuDetails`
--

DROP TABLE IF EXISTS `restMenuDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restMenuDetails` (
  `restaurantId` int DEFAULT NULL,
  `foodItemId` int NOT NULL AUTO_INCREMENT,
  `foodItemName` varchar(50) DEFAULT NULL,
  `foodItemPrice` double DEFAULT NULL,
  `foodItemAvailable` int DEFAULT NULL,
  PRIMARY KEY (`foodItemId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `restMenuDetails_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurantInfo` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restMenuDetails`
--

LOCK TABLES `restMenuDetails` WRITE;
/*!40000 ALTER TABLE `restMenuDetails` DISABLE KEYS */;
INSERT INTO `restMenuDetails` VALUES (1,1,'Paneer chilli',120,1),(1,2,'Dal Makhani',150,1),(1,3,'Chicken Tikka Masala',200,1),(1,4,'Cumin Rice',130,0),(1,5,'Chicken Dum Biryani',180,1),(4,6,'Paneer chilli',150,NULL),(4,7,'Dal Makhani',180,NULL),(4,8,'Chicken Tikka Masala',230,NULL),(4,9,'Daal Rice',120,NULL),(4,10,'Chicken Dum Biryani',250,NULL),(3,11,'Paneer chilli',120,NULL),(3,12,'Dal Makhani',160,NULL),(3,13,'Veg Tikka Masala',180,NULL),(3,14,'Chicken Dum Biryani',150,NULL),(2,15,'Paneer chilli',120,1),(2,16,'Dal Makhani',150,1),(2,17,'Chicken Tikka Masala',200,1),(2,18,'Cumin Rice',130,1),(2,19,'Chicken Dum Biryani',180,1);
/*!40000 ALTER TABLE `restMenuDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restPicDetails`
--

DROP TABLE IF EXISTS `restPicDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restPicDetails` (
  `restaurantId` int DEFAULT NULL,
  `restPic` varchar(100) DEFAULT NULL,
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `restPicDetails_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurantInfo` (`restaurantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restPicDetails`
--

LOCK TABLES `restPicDetails` WRITE;
/*!40000 ALTER TABLE `restPicDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `restPicDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restTableDetails`
--

DROP TABLE IF EXISTS `restTableDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restTableDetails` (
  `restaurantId` int DEFAULT NULL,
  `restNoOfTableReserved` int DEFAULT NULL,
  `restTotalNoOfTables` int DEFAULT NULL,
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `restTableDetails_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurantInfo` (`restaurantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restTableDetails`
--

LOCK TABLES `restTableDetails` WRITE;
/*!40000 ALTER TABLE `restTableDetails` DISABLE KEYS */;
INSERT INTO `restTableDetails` VALUES (1,NULL,10),(4,NULL,15),(3,NULL,12),(2,NULL,20);
/*!40000 ALTER TABLE `restTableDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurantInfo`
--

DROP TABLE IF EXISTS `restaurantInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurantInfo` (
  `restaurantId` int NOT NULL AUTO_INCREMENT,
  `restName` varchar(50) DEFAULT NULL,
  `restAdminName` varchar(50) DEFAULT NULL,
  `restMobileNo` varchar(15) DEFAULT NULL,
  `restEmail` varchar(50) DEFAULT NULL,
  `restAddressArea` varchar(50) DEFAULT NULL,
  `restAddressStreet` varchar(50) DEFAULT NULL,
  `restAddressLandmark` varchar(50) DEFAULT NULL,
  `cityPin` int DEFAULT NULL,
  `restDescription` varchar(300) DEFAULT NULL,
  `restOpenTime` time DEFAULT NULL,
  `restCloseTime` time DEFAULT NULL,
  `restNoOfTables` int DEFAULT NULL,
  `restPassword` varchar(100) DEFAULT NULL,
  `restLoggedInStatus` int DEFAULT '0',
  `active` int DEFAULT '0',
  `activationToken` varchar(100) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`restaurantId`),
  UNIQUE KEY `restEmail` (`restEmail`),
  KEY `cityPin` (`cityPin`),
  CONSTRAINT `restaurantInfo_ibfk_1` FOREIGN KEY (`cityPin`) REFERENCES `pinInfo` (`cityPin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurantInfo`
--

LOCK TABLES `restaurantInfo` WRITE;
/*!40000 ALTER TABLE `restaurantInfo` DISABLE KEYS */;
INSERT INTO `restaurantInfo` VALUES (1,'Restaurant Chinmay','Chinmay Borgaonkar','7776854477','borgaonkarchinmays@gmail.com','Baner','Sakal Nagar','Pashan Link road',431007,'This is nice restaurant with beautiful ambience.','10:00:00','22:00:00',10,'d5c651f5b75daecd3ca78720e8d758b384be341c2506a222e49d90bdf2e54703',0,1,'','2021-01-17 18:25:41'),(2,'Restaurant Rasik','Rasika Satpute','8356077022','rasikasatpute5@gmail.com','Viman Nagar','Viman Nagar Road','Phenoix Mall',431007,'This is nice restaurant with beautiful ambience.','10:00:00','22:00:00',20,'6259f2b6fdd1b3805f0c202644fa4b1ab0588a5210fdd6d9e7bf8e9f2e5ad888',0,1,'','2021-01-17 18:41:19'),(3,'Restaurant Vrushali','Vrushali Jagtap','9130821163','vrushali.jagtap199@gmail.com','Aundh','Parihar chawk','WestEnd Mall',431007,'This is nice restaurant with beautiful ambience.','10:00:00','23:00:00',12,'1d00c58f2842787b3491b6f1a3ef7242d00ba63aedae45f6612dfcf7babc2f0d',0,1,'','2021-01-17 18:43:25'),(4,'Restaurant Vartika','Vartika Modi','8982365479','vartikamodi97@gmail.com','Peth','Market yard','SB road ',431007,'This is nice restaurant with beautiful ambience.','10:00:00','22:00:00',15,'15b546680add10c15f1ed58d3a7b55dbbfd25891bbe31c628d71e23b6e70b4d5',0,1,'','2021-01-17 18:44:36');
/*!40000 ALTER TABLE `restaurantInfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-18 13:08:14

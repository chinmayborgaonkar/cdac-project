import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  urlCustomer = "http://35.153.50.100:3000/customer"


  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { }

  SearchByCity(city) {
    const body = {
      city: city
    }
    return this.httpClient.post(this.urlCustomer + '/hotel/city', body)
  }

  SearchByPrice(startRange, endRange) {
    const body = {
      startRange: startRange,
      endRange: endRange
    }
    return this.httpClient.post(this.urlCustomer + '/hotel/price', body)
  }

  SearchByName(hotelName) {
    const body = {
      hotelName: hotelName
    }
    return this.httpClient.post(this.urlCustomer + '/hotel/name', body)
  }



  SearchByFoodItem(foodItem) {
    const body = {
      foodItem: foodItem
    }

    return this.httpClient.post(this.urlCustomer + '/restaurant/menuItemsName', body)
  }

  SearchByNameR(hotelName1) {
    const body = {
      hotelName: hotelName1
    }
    return this.httpClient.post(this.urlCustomer + '/restaurant/name', body)
  }

  SearchByCityR(city) {
    const body = {
      city: city
    }
    return this.httpClient.post(this.urlCustomer + '/restaurant/city', body)
  }
}

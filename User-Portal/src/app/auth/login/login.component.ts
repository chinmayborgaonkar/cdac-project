import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { CustomerModule } from 'src/app/customer/customer.module';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  role = '';
  email = '';
  password = '';

  constructor(private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
  }

  login() {
    if (this.email.length == 0) {
      this.toastr.error("Enter Email");
    }
    else if (this.password.length == 0) {
      this.toastr.error("Enter password")
    } else {
      if (this.role == 'customer') {
        console.log("customer login")
        this.authService
          .CustomerLogin(this.email, this.password)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const data = response['data']
              console.log(data)

              // cache the user info
              sessionStorage['token'] = data['token']
              sessionStorage['firstName'] = data['firstName']
              sessionStorage['lastName'] = data['lastName']
              sessionStorage['role'] = data['role']

              this.toastr.success(`Welcome ${data['firstName']} to Our Application`)

              // goto the dashboard
              this.router.navigate(['/'])

            } else {
              this.toastr.error(response['error'])
            }
          })
      }
      else if (this.role == 'hotel') {
        console.log("hotel login")
        this.authService
          .HotelLogin(this.email, this.password)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const data = response['data']
              console.log(data)

              // cache the user info
              sessionStorage['token'] = data['token']
              sessionStorage['hotelName'] = data['hotelName']
              sessionStorage['hotelAdminName'] = data['hotelAdminName']
              sessionStorage['role'] = data['role']

              this.toastr.success(`Welcome ${data['hotelName']} to Our Application`)

              // goto the dashboard
              this.router.navigate(['/hotel/hotel-dashboard'])

            } else {
              this.toastr.error(response['error'])
            }
          })
      }
      else if (this.role == 'restaurant') {
        console.log("Restaurant login")
        this.authService
          .RestaurantLogin(this.email, this.password)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const data = response['data']
              console.log(data)

              // cache the user info
              sessionStorage['token'] = data['token']
              sessionStorage['restName'] = data['restName']
              sessionStorage['restAdminName'] = data['restAdminName']
              sessionStorage['role'] = data['role']

              this.toastr.success(`Welcome ${data['restName']} to Our Application`)

              // goto the dashboard
              this.router.navigate(['/restaurant/restaurant-dashboard'])

            } else {
              this.toastr.error(response['error'])
            }
          })
      }
      else {
        alert("Enter Valid Option");
      }

    }



  }


}


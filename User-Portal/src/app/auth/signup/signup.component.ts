import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  role = ''
  //customer
  custFirstName = ''
  custLastName = ''
  custAddress = ''
  custMobileNo
  custEmail = ''
  cityPin
  custPassword = ''

  //hotel
  hotelName = ''
  hotelAdminName = ''
  hotelMobileNo
  hotelEmail = ''
  hotelAddressArea = ''
  hotelAddressStreet = ''
  hotelAddressLandmark = ''
  cityPinH = ''
  hotelDescription = ''
  hotelNoOfRooms
  hotelPassword = ''

  //restuarant
  restName = ''
  restAdminName = ''
  restMobileNo
  restEmail = ''
  restAddressArea = ''
  restAddressStreet = ''
  restAddressLandmark = ''
  cityPinR
  restDescription = ''
  restOpenTime = ''
  restCloseTime = ''
  restNoOfTables
  restPassword = ''

  constructor(private authService: AuthService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  signup() {
    if (this.role == 'customer') {
      if ((this.custFirstName == '' ||
        this.custLastName == '' ||
        this.custAddress == '' ||
        this.custMobileNo == null ||
        this.custEmail == '' ||
        this.cityPin == null ||
        this.custPassword == '')) {
        this.toastr.error('All fields are mandatory')
      } else {
        console.log("customer login")
        this.authService
          .CustomerSignup(this.custFirstName,
            this.custLastName,
            this.custAddress,
            this.custMobileNo,
            this.custEmail,
            this.cityPin,
            this.custPassword)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const data = response['data']
              console.log(data)
              this.toastr.success(`signup successful. Please check your mail`)

              // goto the dashboard
              this.router.navigate(['/'])

            } else {
              this.toastr.error(response['error'])
            }
          })
      }

    }
    else if (this.role == 'hotel') {
      if (this.hotelName == '' ||
        this.hotelAdminName == '' ||
        this.hotelMobileNo == null ||
        this.hotelEmail == '' ||
        this.hotelAddressArea == '' ||
        this.hotelAddressStreet == '' ||
        this.hotelAddressLandmark == '' ||
        this.cityPinH == null ||
        this.hotelDescription == '' ||
        this.hotelNoOfRooms == null ||
        this.hotelPassword == '') {
        this.toastr.error('All fields are mandatory')
      }
      else {
        console.log("hotel login")
        this.authService
          .HotelSignup(this.hotelName, this.hotelAdminName, this.hotelMobileNo, this.hotelEmail, this.hotelAddressArea, this.hotelAddressStreet, this.hotelAddressLandmark, this.cityPinH, this.hotelDescription, this.hotelNoOfRooms, this.hotelPassword)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const data = response['data']
              console.log(data)
              this.toastr.success(`signup successful please check your mail`)

              // goto the dashboard
              this.router.navigate(['/'])

            } else {
              this.toastr.error(response['error'])
            }
          })
      }

    }
    else if (this.role == 'restaurant') {
      if ((this.restName == '' ||
        this.restAdminName == '' ||
        this.restMobileNo == null ||
        this.restEmail == '' ||
        this.restAddressArea == '' ||
        this.restAddressStreet == '' ||
        this.restAddressLandmark == '' ||
        this.cityPinR == null ||
        this.restDescription == '' ||
        this.restOpenTime == '' ||
        this.restCloseTime == '' ||
        this.restNoOfTables == null ||
        this.restPassword == '')) {
        this.toastr.error('All fields are mandatory')
      }
      else {
        console.log("restaurant login")
        this.authService
          .RestaurantSignup(this.restName, this.restAdminName, this.restMobileNo, this.restEmail, this.restAddressArea, this.restAddressStreet, this.restAddressLandmark, this.cityPinR, this.restDescription, this.restOpenTime, this.restCloseTime, this.restNoOfTables, this.restPassword)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const data = response['data']
              console.log(data)

              this.toastr.success(`signup successful please check your mail`)

              // goto the dashboard
              this.router.navigate(['/'])

            } else {
              this.toastr.error(response['error'])
            }
          })
      }

    }
    else {
      this.toastr.error("Enter Valid Option");
    }
  }

}

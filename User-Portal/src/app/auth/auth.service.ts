import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  urlCustomer = "http://35.153.50.100:3000/customer"
  urlHotel = "http://35.153.50.100:4000/hotel"
  urlRestaurant = "http://35.153.50.100:4000/restaurant"

  constructor(
    private router: Router,
    private httpClient: HttpClient) { }

  CustomerLogin(email: string, password: string) {
    const body = {
      email: email,
      password: password
    }
    return this.httpClient.post(this.urlCustomer + '/signin', body)
  }

  HotelLogin(email: string, password: string) {
    const body = {
      email: email,
      password: password
    }
    return this.httpClient.post(this.urlHotel + '/signin', body)
  }

  RestaurantLogin(email: string, password: string) {
    const body = {
      email: email,
      password: password
    }
    return this.httpClient.post(this.urlRestaurant + '/signin', body)
  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage['token']) {
      return true
    }

    this.router.navigate(['/auth/login'])
    return false
  }

  CustomerSignup(custFirstName, custLastName, custAddress, custMobileNo, custEmail, cityPin, custPassword) {
    const body = {
      custFirstName: custFirstName,
      custLastName: custLastName,
      custAddress: custAddress,
      custMobileNo: custMobileNo,
      custEmail: custEmail,
      cityPin: cityPin,
      custPassword: custPassword
    }

    return this.httpClient.post(this.urlCustomer + '/signup', body)
  }


  HotelSignup(hotelName, hotelAdminName, hotelMobileNo, hotelEmail, hotelAddressArea, hotelAddressStreet, hotelAddressLandmark, cityPin, hotelDescription, hotelNoOfRooms, hotelPassword) {
    const body = {
      hotelName: hotelName,
      hotelAdminName: hotelAdminName,
      hotelMobileNo: hotelMobileNo,
      hotelEmail: hotelEmail,
      hotelAddressArea: hotelAddressArea,
      hotelAddressStreet: hotelAddressStreet,
      hotelAddressLandmark: hotelAddressStreet,
      cityPin: cityPin,
      hotelDescription: hotelDescription,
      hotelNoOfRooms: hotelNoOfRooms,
      hotelPassword: hotelPassword
    }
    return this.httpClient.post(this.urlHotel + "/signup", body)
  }

  RestaurantSignup(restName, restAdminName, restMobileNo, restEmail, restAddressArea, restAddressStreet, restAddressLandmark, cityPin, restDescription, restOpenTime, restCloseTime, restNoOfTables, restPassword) {
    const body = {
      restName: restName,
      restAdminName: restAdminName,
      restMobileNo: restMobileNo,
      restEmail: restEmail,
      restAddressArea: restAddressArea,
      restAddressStreet: restAddressStreet,
      restAddressLandmark: restAddressLandmark,
      cityPin: cityPin,
      restDescription: restDescription,
      restOpenTime: restOpenTime,
      restCloseTime: restCloseTime,
      restNoOfTables: restNoOfTables,
      restPassword: restPassword
    }
    return this.httpClient.post(this.urlRestaurant + "/signup", body)
  }
}


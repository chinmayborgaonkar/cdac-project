import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-service-edit',
  templateUrl: './service-edit.component.html',
  styleUrls: ['./service-edit.component.css']
})
export class ServiceEditComponent implements OnInit {

  services = []
  hotelServiceName = ''
  hotelServiceAvailable = 0

  constructor(
    private router: Router,
    private hotelService: HotelService
  ) { }

  ngOnInit(): void {
    this.loadServices()
  }

  toggleActive(service) {
    this.hotelService
      .toggleActiveStatus(service)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadServices()
        } else {
          console.log(service['hotelServiceName'])
          console.log(service['hotelServiceAvailable'])
          console.log(response['error'])
        }
      })
  }

  loadServices() {
    this.hotelService
      .getServices()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.services = response['data']
          console.log(this.services)
        } else {
          console.log(response['error'])
        }
      })
  }

  AddService() {
    this.router.navigate(['/hotel/add-service'])
  }





}

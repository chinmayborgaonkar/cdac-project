import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  url = 'http://35.153.50.100:4000/hotel'
  urlHotelRoom = "http://35.153.50.100:4000/hotel/room"

  constructor(private httpClient: HttpClient) { }

  //================================ GET PROFILE DETAILS ======================================

  getProfileDetails() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/get-profile", httpOptions)
  }

  //================================ GET OTHER PROFILE DETAILS ======================================

  getOtherProfileDetails() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/get-other-info", httpOptions)
  }

  //================================ UPDATE HOTEL PROFILE ======================================
  updateProfile(hotelName: string, hotelAdminName: string, hotelMobileNo: string, hotelEmail: string, hotelAddressArea: string, hotelAddressStreet: string, hotelAddressLandmark: string, hotelcityPin: number, hotelDescription: string, hotelNoOfRooms: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      hotelName: hotelName,
      hotelAdminName: hotelAdminName,
      hotelMobileNo: hotelMobileNo,
      hotelEmail: hotelEmail,
      hotelAddressArea: hotelAddressArea,
      hotelAddressStreet: hotelAddressStreet,
      hotelAddressLandmark: hotelAddressLandmark,
      cityPin: hotelcityPin,
      hotelDescription: hotelDescription,
      hotelNoOfRooms: hotelNoOfRooms
    }

    return this.httpClient.put(this.url + "/update-profile", body, httpOptions)
  }

  //================================ DELETE HOTEL ACCOUNT ======================================

  deleteHotelAccount() {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {

    }
    return this.httpClient.post(this.url + "/delete-profile", body, httpOptions)
  }

  //================================ TOGGLE ACTIVE STATUS =============================
  toggleActiveStatus(service) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    // suspend service if already active or activate otherwise
    const body = {
      hotelServiceAvailable: service['hotelServiceAvailable'] == 1 ? 0 : 1,
      hotelServiceName: service['hotelServiceName']
    }

    return this.httpClient.put(this.url + "/services/update-services", body, httpOptions)
  }

  //================================ GET SERVICES ======================================
  getServices() {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + "/services/get-services", httpOptions)
  }

  //================================ UPLOAD IMAGE ======================================
  uploadImage(file) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = new FormData()
    body.append('image', file)

    return this.httpClient.post(this.url + `/upload-hotel-pics`, body, httpOptions)
  }

  //================================ CHANGE PASSWORD ======================================

  changePasssword(oldPassword: string, newPassword: string) {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {
      oldPassword: oldPassword,
      newPassword: newPassword
    }
    return this.httpClient.post(this.url + "/reset-password", body, httpOptions)

  }
  //================================ LOAD ROOMS ===============================================
  getRooms() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.urlHotelRoom + '/', httpOptions)
  }

  //===================================== UPDATE ROOM =========================================
  updateRoom(hotelRoomId: number, hotelRoomPrice: number, hotelRoomCategory: string, hotelRoomAccomodation: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      hotelRoomPrice: hotelRoomPrice,
      hotelRoomCategory: hotelRoomCategory,
      hotelRoomAccomodation: hotelRoomAccomodation
    }

    return this.httpClient.put(this.urlHotelRoom + "/" + hotelRoomId, body, httpOptions)
  }

  //=====================================ADD ROOM===========================================
  insertRoom(hotelRoomPrice: number, hotelRoomCategory: string, hotelRoomAccomodation: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      hotelRoomPrice: hotelRoomPrice,
      hotelRoomCategory: hotelRoomCategory,
      hotelRoomAccomodation: hotelRoomAccomodation

    }

    return this.httpClient.post(this.urlHotelRoom + "/", body, httpOptions)
  }

  //=============================GET ROOM DETAILS BY ROOM-CATEGORY==========================
  getRoomDetails(hotelRoomCategory) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.urlHotelRoom + "/", httpOptions)
  }

  //================================DELETE SPECIFIC ROOM================================
  deleteRoom(hotelRoomId: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.httpClient.delete(this.urlHotelRoom + "/single-delete/" + hotelRoomId, httpOptions)
  }
  //==============================DELETE ALL ROOM======================================
  deleteAllRoom() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }

    return this.httpClient.delete(this.url + "/room/delete-all", httpOptions)
  }

  //================================ LOAD BOOKINGS ===============================================
  getBookings() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/booking/all', httpOptions)
  }

  //================================ GET GUEST BY HOTEL BOOKING ID ======================================

  getGuests(hotelBookingId) {
    console.log(hotelBookingId)
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/booking/display-guest-list' + "/" + hotelBookingId, httpOptions)
  }

  //================================ GET CUSTOMER BY BOOKING ID ======================================
  getCustomer(hotelBookingId) {
    console.log(hotelBookingId)
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/booking/custBookingDetails' + "/" + hotelBookingId, httpOptions)
  }

  //================================ ADD SERVICE ===============================================
  insertService(hotelServiceName: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      hotelServiceName: hotelServiceName
    }

    return this.httpClient.post(this.url + "/services/add-service", body, httpOptions)
  }
}


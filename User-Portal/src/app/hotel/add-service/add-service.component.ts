import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.css']
})
export class AddServiceComponent implements OnInit {


  hotelServiceName = ''

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private hotelService: HotelService
  ) { }

  ngOnInit(): void {
  }

  onAddService() {
    if (this.hotelServiceName == '') {
      this.toastr.error('Service name can not be empty')
    }
    else {
      // insert
      this.hotelService
        .insertService(this.hotelServiceName)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/hotel/service-edit'])
            this.toastr.success('Service Added successfully...!!')
          }
        })
    }
  }
}

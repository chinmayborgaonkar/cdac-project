import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  action = ''

  Details = []
  otherDetails = []

  constructor(
    private router: Router,
    private hotelService: HotelService,
    private toastr: ToastrService
  ) { }
  ngOnInit(): void {
    this.loadProfileData()
    this.getOtherDetails()
  }

  loadProfileData() {
    this.hotelService.getProfileDetails().subscribe(response => {
      if (response['status'] == 'success') {
        this.Details = response['data']
        console.log(this.Details)
      }
    })
  }
  onEditProfile() {
    this.router.navigate(['/hotel/profile-edit'])
  }
  onEditService() {
    this.router.navigate(['/hotel/service-edit'])
  }

  onViewPhotos() {
    this.getOtherDetails()
  }

  onDelete() {
    this.hotelService.deleteHotelAccount()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Account temporarily deleted')
          this.router.navigate(['/auth/login'])
        }
        else {
          this.toastr.error('Account deletion failed')
          console.log(response['error'])
        }
      })
  }

  getOtherDetails() {
    this.hotelService.getOtherProfileDetails().subscribe(response => {
      if (response['status'] == 'success') {
        this.otherDetails = response['data']
        console.log(this.otherDetails[0]['hotelPic'])
      } else {
        console.log(response['error'])
      }
    })
  }

  uploadImage() {
    this.router.navigate(['/hotel/upload-image'])
  }



}

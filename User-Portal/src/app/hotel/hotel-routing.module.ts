import { ProfileComponent } from './profile/profile.component'
import { SearchComponent } from './search/search.component'
import { BookingMgmtComponent } from './booking-mgmt/booking-mgmt.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelDashboardComponent } from './hotel-dashboard/hotel-dashboard.component';
import { AuthService } from '../auth/auth.service';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ServiceEditComponent } from './service-edit/service-edit.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AddRoomComponent } from './add-room/add-room.component';
import { RoomManagementComponent } from './room-management/room-management.component';
import { AddServiceComponent } from './add-service/add-service.component';

const routes: Routes = [
  { path: 'booking-mgmt', component: BookingMgmtComponent, canActivate: [AuthService] },
  { path: 'hotel-dashboard', component: HotelDashboardComponent, canActivate: [AuthService] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthService] },
  { path: 'profile-edit', component: ProfileEditComponent, canActivate: [AuthService] },
  { path: 'service-edit', component: ServiceEditComponent, canActivate: [AuthService] },
  { path: 'add-service', component: AddServiceComponent, canActivate: [AuthService] },
  { path: 'upload-image', component: UploadImageComponent, canActivate: [AuthService] },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthService] },
  { path: 'room-mgmt', component: RoomManagementComponent, canActivate: [AuthService] },
  { path: 'add-room', component: AddRoomComponent, canActivate: [AuthService] },
  { path: 'search', component: SearchComponent, canActivate: [AuthService] }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotelRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {


  // hotelRoomPrice 
  // hotelRoomCategory
  // hotelRoomAccomodation

  price = 0
  category = ''
  accomodation = 0
  id = 0
  room

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private hotelService: HotelService
  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.queryParams['hotelRoomId']
    if (this.id) {
      this.hotelService.getRoomDetails(this.id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const rooms = response['data']
            if (rooms.length > 0) {
              for (let i = 0; i < rooms.length; i++) {
                if (rooms[i]['hotelRoomId'] == this.id) {
                  this.room = rooms[i]
                  this.price = this.room['hotelRoomPrice']
                  this.category = this.room['hotelRoomCategory']
                  this.accomodation = this.room['hotelRoomAccomodation']
                }
              }
              // console.log(this.room)
            }
          }
        })
    }
  }


  onUpdate() {
    if (this.room) {
      // edit
      this.hotelService.updateRoom(this.room['hotelRoomId'], this.price, this.category, this.accomodation)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/hotel/room-mgmt'])
            this.toastr.success('Room Updated successfully...!!')
          }
          else {
            this.toastr.error('Room update failed')
          }
        })
    } else {
      if (this.price == null || this.category == '' || this.accomodation == null) {
        this.toastr.error('All fields are mandatory')
      } else {
        // insert
        this.hotelService
          .insertRoom(this.price, this.category, this.accomodation)
          .subscribe(response => {
            if (response['status'] == 'success') {
              this.router.navigate(['/hotel/room-mgmt'])
              this.toastr.success('Room Added successfully...!!')
            }
          })
      }
    }

  }

}

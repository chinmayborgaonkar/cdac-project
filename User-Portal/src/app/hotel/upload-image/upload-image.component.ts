import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HotelService } from '../hotel.service';
@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent implements OnInit {
  selectedFile = null
  product = null

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private hotelService: HotelService,) { }

  ngOnInit(): void {

  }
  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  onUploadImage() {
    this.hotelService
      .uploadImage(this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/hotel/profile'])
        } else {
          console.log(response['error'])
        }
      })
  }

}

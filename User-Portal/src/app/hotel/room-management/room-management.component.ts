import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-room-management',
  templateUrl: './room-management.component.html',
  styleUrls: ['./room-management.component.css']
})
export class RoomManagementComponent implements OnInit {
  rooms = []

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private hotelService: HotelService) { }

  ngOnInit(): void {
    this.loadRooms()
  }

  loadRooms() {
    this.hotelService
      .getRooms()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.rooms = response['data']
          // console.log(this.rooms)
        } else {
          console.log(response['error'])
        }
      })
  }

  deleteAllRoom() {
    this.hotelService
      .deleteAllRoom().subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('All Rooms deleted successfully...!!')
          this.loadRooms()
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(room) {
    this.router.navigate(['/hotel/add-room'], { queryParams: { hotelRoomId: room['hotelRoomId'] } })
  }

  addRoom() {
    this.router.navigate(['/hotel/add-room'])

  }
  onDelete(room) {
    this.hotelService
      .deleteRoom(room['hotelRoomId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Room deleted successfully...!!')
          this.loadRooms()

        } else {
          console.log(response['error'])
        }
      })
  }

}

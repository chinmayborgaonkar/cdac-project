import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {

  hotelName = ''
  hotelAdminName = ''
  hotelMobileNo
  hotelEmail = ''
  hotelAddressArea = ''
  hotelAddressStreet = ''
  hotelAddressLandmark = ''
  hotelcityPin
  hotelDescription = ''
  hotelNoOfRooms

  constructor(
    private router: Router,
    private hotelService: HotelService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.hotelService.getProfileDetails().subscribe(response => {
      if (response['status'] == 'success') {
        const details = response['data']
        console.log(details[0]['hotelName'])
        this.hotelName = details[0]['hotelName']
        this.hotelAdminName = details[0]['hotelAdminName']
        this.hotelMobileNo = details[0]['hotelMobileNo']
        this.hotelEmail = details[0]['hotelEmail']
        this.hotelAddressArea = details[0]['hotelAddressArea']
        this.hotelAddressStreet = details[0]['hotelAddressStreet']
        this.hotelAddressLandmark = details[0]['hotelAddressLandmark']
        this.hotelcityPin = details[0]['cityPin']
        this.hotelDescription = details[0]['hotelDescription']
        console.log(details[0]['hotelDescription'])
        this.hotelNoOfRooms = details[0]['hotelNoOfRooms']
      }
    })
  }

  onUpdate() {
    if (this.hotelName == '' ||
      this.hotelAdminName == '' ||
      this.hotelMobileNo == null ||
      this.hotelEmail == '' ||
      this.hotelAddressArea == '' ||
      this.hotelAddressStreet == '' ||
      this.hotelAddressLandmark == '' ||
      this.hotelcityPin == null ||
      this.hotelDescription == '' ||
      this.hotelNoOfRooms == null) {
      this.toastr.error('All fields are mandatory')
    } else {
      this.hotelService.updateProfile(this.hotelName,
        this.hotelAdminName,
        this.hotelMobileNo,
        this.hotelEmail,
        this.hotelAddressArea,
        this.hotelAddressStreet,
        this.hotelAddressLandmark,
        this.hotelcityPin,
        this.hotelDescription,
        this.hotelNoOfRooms).subscribe
        (response => {
          if (response['status'] == 'success') {
            this.toastr.success("Profile updated successfully..!!")
            this.router.navigate(['/hotel/profile'])
          }
        })
    }

  }

  changePassword() {
    console.log("hii there change password")
    this.router.navigate(['/hotel/change-password'])
  }
}

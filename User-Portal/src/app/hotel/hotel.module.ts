import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HotelRoutingModule } from './hotel-routing.module';
import { HotelDashboardComponent } from './hotel-dashboard/hotel-dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { BookingMgmtComponent } from './booking-mgmt/booking-mgmt.component';
import { SearchComponent } from './search/search.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ServiceEditComponent } from './service-edit/service-edit.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AddRoomComponent } from './add-room/add-room.component';
import { RoomManagementComponent } from './room-management/room-management.component';
import { AddServiceComponent } from './add-service/add-service.component';



@NgModule({
  declarations: [HotelDashboardComponent, ProfileComponent, BookingMgmtComponent, SearchComponent, ProfileEditComponent, ServiceEditComponent, UploadImageComponent, ChangePasswordComponent, AddRoomComponent, RoomManagementComponent, AddServiceComponent],
  imports: [
    CommonModule,
    ToastrModule.forRoot(),
    FormsModule,
    HotelRoutingModule
  ]
})
export class HotelModule { }

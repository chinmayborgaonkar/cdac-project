import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loginCheck = 0;
  active = 0

  constructor(private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('token') == null) {
      this.loginCheck = 0
    }
    else {
      this.loginCheck = 1
    }
    if (sessionStorage['firstName']) {
      this.active = 1
    }
  }

  Logout() {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('restName')
    sessionStorage.removeItem('restAdminName')
    sessionStorage.removeItem('hotelName')
    sessionStorage.removeItem('hotelAdminName')
    sessionStorage.removeItem('role')
    this.loginCheck = 0
    this.toastr.success("Logged Out")
    this.active = 0
    this.router.navigate(['/dashboard'])

  }

  getRestById() {
    this.router.navigate(['/customer/restaurant-details'], { queryParams: { id: 4 } })
  }
}

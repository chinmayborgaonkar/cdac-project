import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: '',
    component: HomeComponent,
    children:
      [
        { path: 'dashboard', component: DashboardComponent },
        { path: 'customer', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule) },
        { path: 'hotel', loadChildren: () => import('./hotel/hotel.module').then(m => m.HotelModule) },
        { path: 'restaurant', loadChildren: () => import('./restaurant/restaurant.module').then(m => m.RestaurantModule) }
      ]
  },
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

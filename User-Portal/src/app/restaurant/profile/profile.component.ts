
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantService } from '../restaurant.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  Details = []
  otherDetails = []
  custProfileImage = null

  restName = ""
  restAdminName = ""
  restMobileNo = ""
  restEmail = ""
  restAddressArea = ""
  restAddressStreet = ""
  restAddressLandmark = ""
  cityPin = 0
  restDescription = ""
  restOpenTime = ""
  restCloseTime = ""
  restNoOfTables = 0
  active = 0

  action = ''

  constructor(
    private router: Router,
    private restService: RestaurantService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadProfile();
    this.getOtherDetails()
  }

  loadProfile() {

    this.restService.getProfile()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.Details = response['data']
          console.log(this.Details)
        } else {
          console.log(response['error'])
        }
      })

  }

  onEditProfile() {
    this.router.navigate(['/restaurant/edit-profile'])
  }
  onEditMenu() {
    this.router.navigate(['/restaurant/edit-menu'])
  }
  // onEdit() {
  //   if (this.action == 'profile') {
  //     this.router.navigate(['/restaurant/edit-profile'])
  //   } else {
  //     this.router.navigate(['/restaurant/edit-menu'])
  //   }
  // }

  onDelete() {
    this.restService.deleteRestAccount()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Account temporarily deleted')
          this.router.navigate(['/auth/login'])
        }
        else {
          this.toastr.error('Account deletion failed')
          console.log(response['error'])
        }
      })
  }

  getOtherDetails() {
    this.restService.getOtherProfileDetail().subscribe(response => {
      if (response['status'] == 'success') {
        this.otherDetails = response['data']
        console.log(this.otherDetails[0]['restPic'])
      } else {
        console.log(response['error'])
      }
    })
  }

  uploadImage() {
    this.router.navigate(['/restaurant/upload-image'])
  }
}

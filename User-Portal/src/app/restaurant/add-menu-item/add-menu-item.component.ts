import { RestaurantService } from '../restaurant.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-menu-item',
  templateUrl: './add-menu-item.component.html',
  styleUrls: ['./add-menu-item.component.css']
})
export class AddMenuItemComponent implements OnInit {

  itemName = ''
  itemPrice = 0

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private restService: RestaurantService
  ) { }

  ngOnInit(): void {
  }

  onAddItem() {
    if (this.itemName == '' || (this.itemPrice == null || this.itemPrice <= 0)) {
      this.toastr.error('Invalid inputs. Please try again!')
    } else {
      // insert
      this.restService
        .insertItems(this.itemName, this.itemPrice)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/restaurant/edit-menu'])
            this.toastr.success('Item Added successfully...!!')
          }
        })
    }

  }


}

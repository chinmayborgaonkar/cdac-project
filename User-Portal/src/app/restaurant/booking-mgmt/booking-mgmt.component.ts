import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-booking-mgmt',
  templateUrl: './booking-mgmt.component.html',
  styleUrls: ['./booking-mgmt.component.css']
})
export class BookingMgmtComponent implements OnInit {
  bookings = []
  guests = []
  guest = null
  customers = []
  customer = null

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private restService: RestaurantService) { }

  ngOnInit(): void {
    this.loadBookings()
  }

  loadBookings() {
    this.restService
      .getBookings()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.bookings = response['data']
          // console.log(this.rooms)
        } else {
          console.log(response['error'])
        }
      })
  }

  onViewGuest(booking) {
    this.restService
      .getGuests(booking['restBookingId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.guests = response['data']

          if (this.guests.length > 0) {
            for (let i = 0; i < this.guests.length; i++) {
              if (this.guests[i]['restBookingId'] == booking['restBookingId']) {
                this.guest = this.guests[i]
              }
            }
          }
        }
      })
  }

  onViewCustomer(booking) {
    this.restService
      .getCustomer(booking['restBookingId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.customers = response['data']

          if (this.customers.length > 0) {
            for (let i = 0; i < this.customers.length; i++) {
              if (this.customers[i]['restBookingId'] == booking['restBookingId']) {
                this.customer = this.customers[i]
              }
            }
          }
        }
      })
  }



}

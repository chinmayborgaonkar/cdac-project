
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../restaurant.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  Name = ''
  AdminName = ''
  Contact
  Email = ''
  AddressArea = ''
  AddressStreet = ''
  AddressLandmark = ''
  cityPin
  Description = ''
  OpenTime = ''
  CloseTime = ''
  NoOfTables

  constructor(
    private toastr: ToastrService,
    private restaurantService: RestaurantService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.restaurantService.getProfile().subscribe(response => {
      if (response['status'] == 'success') {
        const details = response['data']
        this.Name = details[0]['restName']
        this.AdminName = details[0]['restAdminName']
        this.Contact = details[0]['restMobileNo']
        this.Email = details[0]['restEmail']
        this.AddressArea = details[0]['restAddressArea']
        this.AddressStreet = details[0]['restAddressStreet']
        this.AddressLandmark = details[0]['restAddressLandmark']
        this.cityPin = details[0]['cityPin']
        this.Description = details[0]['restDescription']
        this.OpenTime = details[0]['restOpenTime']
        this.CloseTime = details[0]['restCloseTime']
        this.NoOfTables = details[0]['restNoOfTables']

      }
    })
  }



  onUpdate() {
    if (this.Name == '' ||
      this.AdminName == '' ||
      this.Contact == null ||
      this.Email == '' ||
      this.AddressArea == '' ||
      this.AddressStreet == '' ||
      this.AddressLandmark == '' ||
      this.cityPin == null ||
      this.Description == '' ||
      this.OpenTime == '' ||
      this.CloseTime == '' ||
      this.NoOfTables == null) {
      this.toastr.error('All fields are mandatory')
    } else {
      this.restaurantService.updateProfile(this.Name, this.AdminName, this.Contact, this.Email, this.AddressArea, this.AddressStreet, this.AddressLandmark, this.cityPin, this.Description, this.OpenTime, this.CloseTime, this.NoOfTables)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.toastr.success('profile updated successfully...!!!!')
            this.router.navigate(['/restaurant/profile'])
          }
          else {
            this.toastr.error('profile updation failed...!!!!')
            console.log(response['error'])
          }
        })
    }

  }

  changePassword() {
    this.router.navigate(['/restaurant/change-password'])
  }


}

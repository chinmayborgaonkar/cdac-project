import { RestaurantService } from '../restaurant.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-table',
  templateUrl: './add-table.component.html',
  styleUrls: ['./add-table.component.css']
})
export class AddTableComponent implements OnInit {
  Id = 0
  TotalNoOfTables = 0
  table = []
  tablecheck = 0
  TotalNoOfTablesReserved = 0
  id

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private restService: RestaurantService
  ) { }

  ngOnInit(): void {

    this.restService
      .getTables()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.table = response['data']
          if (this.table.length > 0) {
            this.tablecheck = 1
            this.TotalNoOfTables = this.table[0].restTotalNoOfTables
          }
        }
        else {
          console.log(response['error']);
        }
      })
  }

  onUpdate() {
    if (this.table.length > 0) {
      console.log("update tables")
      // edit
      this.restService
        .updateTable(this.TotalNoOfTables)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/restaurant/view-table'])
            this.toastr.success('Table Updated successfully...!!')
          }
        })
    } else {
      // insert
      console.log("inserting tables")
      this.restService
        .insertTable(this.TotalNoOfTables)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/restaurant/view-table'])
            this.toastr.success('Guest Added successfully...!!')
          }
        })
    }

  }



  // onUpdate() {
  //   console.log(this.id)

  //   if (this.id != null) {
  //     console.log(" on update tables")
  //     console.log(this.TotalNoOfTables)
  //     this.restService
  //       .updateTable(this.TotalNoOfTables, this.id)
  //       .subscribe(response => {
  //         if (response['status'] == 'success') {
  //           this.router.navigate(['/restaurant/view-table'])
  //           this.toastr.success('Table Updated successfully...!!')
  //         }
  //         else {
  //           console.log(response['error']);
  //         }
  //       })
  //   } else if (this.id == null) {
  //     // insert
  //     console.log(" on insert tables")
  //     this.restService
  //       .insertTable(this.TotalNoOfTables)
  //       .subscribe(response => {
  //         if (response['status'] == 'success') {
  //           this.router.navigate(['/restaurant/view-table'])
  //           this.toastr.success('Table Added successfully...!!')
  //         }
  //         else {
  //           console.log(response['error']);
  //         }
  //       })
  //   }
  // }

}

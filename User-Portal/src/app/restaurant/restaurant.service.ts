import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  url = 'http://35.153.50.100:4000/restaurant'
  urlTable = "http://35.153.50.100:4000/restaurant/table"
  urlBooking = "http://35.153.50.100:4000/restaurant/booking"
  urlFood = "http://35.153.50.100:4000/restaurant/food"
  constructor(
    private httpClient: HttpClient
  ) { }



  getProfile() {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/get-profile', httpOptions)

  }

  updateProfile(Name: string, AdminName: string, Contact: string, Email: string, AddressArea: string, AddressStreet: string, AddressLandmark: string, cityPin: number, Description: string, OpenTime: string, CloseTime: string, NoOfTables: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      restName: Name,
      restAdminName: AdminName,
      restMobileNo: Contact,
      restEmail: Email,
      restAddressArea: AddressArea,
      restAddressStreet: AddressStreet,
      restAddressLandmark: AddressLandmark,
      cityPin: cityPin,
      restDescription: Description,
      restOpenTime: OpenTime,
      restCloseTime: CloseTime,
      restNoOfTables: NoOfTables
    }
    return this.httpClient.put(this.url + "/update-profile", body, httpOptions)
  }

  deleteRestAccount() {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {

    }
    return this.httpClient.post(this.url + "/delete-profile", body, httpOptions)
  }

  toggleActiveStatus(item) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      foodItemAvailable: item['foodItemAvailable'] == 1 ? 0 : 1,
      foodItemName: item['foodItemName']
    }

    return this.httpClient.put(this.url + "/food/update-menu", body, httpOptions)
  }


  getItems() {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.urlFood + "/getAll", httpOptions)
  }

  uploadImage(file) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = new FormData()
    body.append('image', file)

    return this.httpClient.post(this.url + `/upload-rest-pics/`, body, httpOptions)
  }

  getOtherProfileDetail() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/get-other-info", httpOptions)
  }

  changePasssword(oldPassword: string, newPassword: string) {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {
      oldPassword: oldPassword,
      newPassword: newPassword
    }
    return this.httpClient.post(this.url + "/reset-password", body, httpOptions)

  }
  //================================LOAD TABLES===============================================
  getTables() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.urlTable + "/", httpOptions)
  }

  //=====================================UPDATE TABLE=========================================
  updateTable(TotalNoOfTables) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = { noOfTables: TotalNoOfTables }
    console.log(httpOptions)
    console.log(body)
    return this.httpClient.put(this.urlTable + "/", body, httpOptions)
  }


  //=====================================ADD TABLE===========================================
  insertTable(TotalNoOfTables) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      noOfTables: TotalNoOfTables
    }
    console.log(httpOptions)
    console.log(body)
    return this.httpClient.post(this.urlTable + "/", body, httpOptions)
  }

  //===================================GET TABLE DETAILS===================================
  getTableDetails(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.urlTable + "/", httpOptions)
  }

  getBookings() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.urlBooking + "/all", httpOptions)
  }

  getGuests(restBookingId) {
    console.log(restBookingId)
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/booking/display-guest-list' + "/" + restBookingId, httpOptions)
  }

  getCustomer(restBookingId) {
    console.log(restBookingId)
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + '/booking/custBookingDetails' + "/" + restBookingId, httpOptions)
  }

  insertItems(name: string, price: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      name: name,
      price: price

    }

    return this.httpClient.post(this.urlFood + "/add", body, httpOptions)
  }

}




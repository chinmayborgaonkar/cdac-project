import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  oldPassword = ''
  newPassword = ''
  constructor(
    private toastr: ToastrService,
    private restService: RestaurantService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  changePassword() {
    if (this.oldPassword == '' || this.newPassword == '') {
      this.toastr.error('Password can not be empty')
    } else {
      this.restService.changePasssword(this.oldPassword, this.newPassword).subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Password updated successfully...!!!!')
          this.router.navigate(['/restaurant/profile'])
        }
        else {
          this.toastr.error('Password updated successfully...!!!!')
          console.log(response['error'])
        }
      })
    }
  }
}

import { RestaurantService } from '../restaurant.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-view-table',
  templateUrl: './view-table.component.html',
  styleUrls: ['./view-table.component.css']
})
export class ViewTableComponent implements OnInit {

  tables = []
  tablecheck = 0

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private restService: RestaurantService
  ) { }

  ngOnInit(): void {
    this.loadTables()
  }

  loadTables() {
    this.restService
      .getTables()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.tables = response['data']
          if (this.tables.length > 0)
            this.tablecheck = 1
          console.log(this.tables)
        } else {
          console.log(response['error'])
        }
      })
  }

  addTables() {
    this.router.navigate(['/restaurant/add-table'])
  }
}

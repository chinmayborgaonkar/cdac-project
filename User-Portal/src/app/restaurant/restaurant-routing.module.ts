import { AuthService } from './../auth/auth.service';
import { ServiceMgmtComponent } from './service-mgmt/service-mgmt.component';
import { SearchComponent } from './search/search.component';
import { BookingMgmtComponent } from './booking-mgmt/booking-mgmt.component';
import { ProfileComponent } from './profile/profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantDashboardComponent } from './restaurant-dashboard/restaurant-dashboard.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AddTableComponent } from './add-table/add-table.component';
import { ViewTableComponent } from './view-table/view-table.component';
import { EditTableComponent } from './edit-table/edit-table.component';
import { ViewBookingDetailsComponent } from './view-booking-details/view-booking-details.component';
import { AddMenuItemComponent } from './add-menu-item/add-menu-item.component';



const routes: Routes = [
  { path: 'profile', component: ProfileComponent, canActivate: [AuthService] },
  { path: 'booking-mgmt', component: BookingMgmtComponent, canActivate: [AuthService] },
  { path: 'restaurant-dashboard', component: RestaurantDashboardComponent, canActivate: [AuthService] },
  { path: 'search', component: SearchComponent, canActivate: [AuthService] },
  { path: 'service-mgmt', component: ServiceMgmtComponent, canActivate: [AuthService] },
  { path: 'edit-profile', component: EditProfileComponent, canActivate: [AuthService] },
  { path: 'edit-menu', component: EditMenuComponent, canActivate: [AuthService] },
  { path: 'upload-image', component: UploadImageComponent, canActivate: [AuthService] },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthService] },
  { path: 'add-table', component: AddTableComponent, canActivate: [AuthService] },
  { path: 'view-table', component: ViewTableComponent, canActivate: [AuthService] },
  { path: 'edit-table', component: EditTableComponent, canActivate: [AuthService] },
  { path: 'view-booking', component: ViewBookingDetailsComponent, canActivate: [AuthService] },
  { path: 'add-menu-item', component: AddMenuItemComponent, canActivate: [AuthService] }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantRoutingModule { }

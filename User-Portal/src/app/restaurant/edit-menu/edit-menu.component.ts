import { RestaurantService } from '../restaurant.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.css']
})
export class EditMenuComponent implements OnInit {

  items = []
  foodItemName = ''
  foodItemAvailable = 0

  constructor(
    private router: Router,
    private restService: RestaurantService
  ) { }

  ngOnInit(): void {
    this.loadItems()
  }


  toggleActive(item) {
    this.restService
      .toggleActiveStatus(item)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadItems()
        } else {
          console.log(item['foodItemName'])
          console.log(item['foodItemAvailable'])
          console.log(response['error'])
        }
      })
  }

  loadItems() {
    this.restService
      .getItems()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.items = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

  AddItem() {
    this.router.navigate(['/restaurant/add-menu-item'])
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestaurantRoutingModule } from './restaurant-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { RestaurantDashboardComponent } from './restaurant-dashboard/restaurant-dashboard.component';
import { SearchComponent } from './search/search.component';
import { BookingMgmtComponent } from './booking-mgmt/booking-mgmt.component';
import { ServiceMgmtComponent } from './service-mgmt/service-mgmt.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AddTableComponent } from './add-table/add-table.component';
import { ViewTableComponent } from './view-table/view-table.component';
import { EditTableComponent } from './edit-table/edit-table.component';
import { ViewBookingDetailsComponent } from './view-booking-details/view-booking-details.component';
import { AddMenuItemComponent } from './add-menu-item/add-menu-item.component';




@NgModule({
  declarations: [ProfileComponent, RestaurantDashboardComponent, SearchComponent, BookingMgmtComponent, ServiceMgmtComponent, EditProfileComponent, EditMenuComponent, UploadImageComponent, ChangePasswordComponent, AddTableComponent, ViewTableComponent, EditTableComponent, ViewBookingDetailsComponent, AddMenuItemComponent],
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    FormsModule,
    HttpClientModule,
  ]
})
export class RestaurantModule { }

import { HomeService } from './../home.service';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService, ToastrModule } from 'ngx-toastr';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  values = ''
  role = ''
  startRange = ''
  endRange = ''
  successResponse = false;
  data = '';
  role1 = ''
  foodItem = ''
  profileShow = 0
  tabChange = 0
  activeTab = 'hotel'

  constructor(private homeService: HomeService,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    if (sessionStorage.length > 0)
      this.profileShow = 1
  }

  search(activeTab) {
    this.activeTab = activeTab;
    this.successResponse = false;
    console.log(this.activeTab)
  }

  onValueChange(event: any) {
    this.values = event.target.value;
    console.log(this.values);
  }

  SearchByHotel() {
    if (this.role == 'city' && this.values != '') {
      this.homeService
        .SearchByCity(this.values)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.data = response['data']
            console.log(this.data)
            if (this.data.length != 0) {
              this.successResponse = true;
            } else { this.successResponse = false; }
          } else {
            this.toastr.error(response['error'])
            this.successResponse = false;
          }
        })
    }
    else if (this.role == 'price') {
      this.homeService
        .SearchByPrice(this.startRange, this.endRange)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.data = response['data']
            console.log(this.data)
            if (this.data.length != 0) {
              this.successResponse = true;
            } else { this.successResponse = false; }
          } else {
            this.toastr.error(response['error'])
            this.successResponse = false;
          }
        })
    } else if (this.role == 'nameH' && this.values != '') {
      this.homeService
        .SearchByName(this.values)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.data = response['data']
            console.log(this.data)
            if (this.data.length != 0) {
              this.successResponse = true;
            } else { this.successResponse = false; }

          } else {
            this.toastr.error(response['error'])
            this.successResponse = false;
          }
        })
    }
    else {
      this.toastr.error(`no results`)
    }
  }

  SearchByRest() {
    if (this.role1 == 'city' && this.values != '') {
      this.homeService
        .SearchByCityR(this.values)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.data = response['data']
            console.log(this.data)
            if (this.data.length != 0) {
              this.successResponse = true;
            } else { this.successResponse = false; }
          } else {
            this.toastr.error(response['error'])
            this.successResponse = false;
          }
        })
    }
    else if (this.role1 == 'foodItem' && this.values != '') {
      console.log(this.values)
      this.homeService
        .SearchByFoodItem(this.values)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.data = response['data']
            console.log(this.data)
            if (this.data.length != 0) {
              this.successResponse = true;
            } else { this.successResponse = false; }
          } else {
            this.toastr.error(response['error'])
            this.successResponse = false;
          }
        })
    } else if (this.role1 == 'nameR' && this.values != '') {
      console.log(this.values)
      this.homeService
        .SearchByNameR(this.values)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.data = response['data']
            console.log(this.data)
            if (this.data.length != 0) {
              this.successResponse = true;
            } else { this.successResponse = false; }

          } else {
            this.toastr.error(response['error'])
            this.successResponse = false;
          }
        })
    }
    else {
      this.toastr.error(`no results`)
    }
  }

  onHotelSelect(hotelData) {
    this.router.navigate(['/customer/hotel-details', { id: hotelData['hotelId'] }])
  }

  onRestSelect(restData) {
    this.router.navigate(['/customer/restaurant-details', { id: restData['restaurantId'] }])
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustHotelService } from '../cust-hotel.service';
import { CustRestaurantService } from '../cust-restaurant.service';

@Component({
  selector: 'app-hotel-booking',
  templateUrl: './hotel-booking.component.html',
  styleUrls: ['./hotel-booking.component.css']
})
export class HotelBookingComponent implements OnInit {
  public sub: any;
  name = ''
  date = null
  status = ''
  noOfTables = 0
  Detail = []
  id = 0
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private custhotelService: CustHotelService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.sub = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.sub)
    this.custhotelService.getHotelBooking(this.sub).subscribe(response => {
      if (response['status'] == 'success') {
        this.Detail = response['data']
        console.log(this.Detail)

      } else {
        console.log(response['error'])
      }
    })
  }

  giveRating() {
    this.router.navigate(['customer/hotel-rating', { id: this.Detail[0]['hotelBookingId'] }])
  }

}

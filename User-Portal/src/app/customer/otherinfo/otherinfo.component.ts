import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-otherinfo',
  templateUrl: './otherinfo.component.html',
  styleUrls: ['./otherinfo.component.css']
})
export class OtherinfoComponent implements OnInit {
  status = ''
  age = 0
  date = null
  gender = ''
  details = []
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {

    this.customerService.getOtherDetail()
      .subscribe(response => {
        if (response['status'] == 'success') {
          const details = response['data']
          if (details.length > 0) {
            this.details = details[0]
            this.status = this.details['custMaritalStatus']
            this.age = this.details['custAge']
            this.date = this.details['custBirthDate']
            this.gender = this.details['custGender']
          }
        }
      })
  }

  onUpdate() {
    if (this.status == '' || this.age == null || this.date == null || this.gender == '') {
      this.toastr.error('All fields are mandatory')
    } else {
      this.customerService
        .updateOtherInfo(this.status, this.age, this.date, this.gender)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/customer/profile'])
            this.toastr.success('Information Updated successfully')
          }
        })
    }
  }


}

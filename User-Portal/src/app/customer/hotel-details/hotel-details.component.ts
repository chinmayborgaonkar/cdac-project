import { CustHotelService } from './../cust-hotel.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HomeService } from 'src/app/home.service';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.css']
})


export class HotelDetailsComponent implements OnInit {
  hotelName = ''
  hotelid
  Details
  RoomData
  hotelPics
  action
  cityPin
  NoOfRooms
  checkindate
  checkoutdate
  BookingDetails
  NoOfGuests = 0
  selectedItemsList
  guestCounter = 0
  guestArray = []
  isValidDate

  BookNow = 0
  guests = []
  bookingsarray = []
  result = 0
  roomsRemaining = 0

  constructor(private custHotelService: CustHotelService,
    private router: Router,
    private customerService: CustomerService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.hotelid = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.hotelid)
    this.loadHotel()
    this.loadGuests()
    this.loadAllBookingHDetails()
  }

  loadOninit() {
    this.roomsRemaining = 0
    this.result = 0
    this.bookingsarray
    const noofroom = this.Details[0].hotelNoOfRooms //10
    for (let book of this.bookingsarray) {
      if (this.checkindate >= book.custCheckInDate && this.checkindate <= book.custCheckOutDate) {
        this.result += book.custNoOfRooms
      }
    }
    if (this.result > noofroom)
      this.toastr.error(" rooms not available");
    else {
      this.roomsRemaining = noofroom - this.result
    }
  }

  loadHotel() {
    this.custHotelService.getHotelDetails(this.hotelid).subscribe(response => {
      if (response['status'] == 'success') {
        console.log(response['data']);
        this.Details = response['data']['hotelDetails']
        this.hotelPics = response['data']['hotelPics']
        this.RoomData = response['data']['Roomdata']
        this.cityPin = this.Details[0]['cityPin']
        if (sessionStorage.length > 0)
          this.BookNow = 1
      } else {
        console.log(response['error'])
      }
    })
  }

  loadAllBookingHDetails() {
    this.custHotelService
      .getHotelBookingDetails(this.hotelid)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.bookingsarray = response['data']
          console.log(this.bookingsarray)
        } else {
          console.log(response['error'])
        }
      })
  }



  goToGuests() {
    this.router.navigate(['/customer/guests'])
  }

  loadGuests() {
    this.customerService
      .getGuests()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.guestArray = response['data']
          console.log(this.guestArray)
          if (this.guestArray.length > 0) {
            this.guestCounter = 1;
          }
          else {
            this.guestCounter = 0
          }

        } else {
          console.log(response['error'])
        }
      })
  }


  toggleEditable(event: any, item: any) {
    if (event.target.checked) {
      this.guests.push({
        custHotelGuestsName: item.custGuestName, custHotelGuestsAge: item.custGuestAge, custHotelGuestsGender: item.custGuestGender
      })
    }
    else {
      const index = this.guests.findIndex(b => b.custHotelGuestsName == item.custGuestName);
      console.log(index)
      if (index != -1) {
        this.guests.splice(index, 1);
      }
    }
    console.log(this.guests)
  }


  goBackToPreviousPage() {
    this.router.navigate(['/'])
  }


  BookTable() {
    if (this.BookNow == 1) {
      this.action = 1
    }
    else {
      this.toastr.success("Please Login to book room")
    }
  }


  showmenu() {
    this.action = 2
  }


  addBooking() {
    if (this.NoOfRooms > this.roomsRemaining) {
      if (this.roomsRemaining > 0)
        this.toastr.error("cannot book room more than " + this.roomsRemaining);
      else {
        this.toastr.error("no room available");
      }
    }
    else {
      if (this.checkindate == null ||
        this.checkoutdate == null ||
        this.NoOfRooms == null ||
        this.cityPin == null ||
        this.guests == null) {
        this.toastr.error('All fields are mandatory')
      } else {
        this.NoOfGuests = this.guests.length;
        this.custHotelService.addBooking(this.Details[0]['hotelId'], this.Details[0]['hotelName'], this.checkindate, this.checkoutdate, this.NoOfGuests, this.NoOfRooms, this.cityPin, this.guests).subscribe(response => {
          this.isValidDate = this.validateDates(this.checkindate, this.checkoutdate);
          if (this.isValidDate) {
            if (response['status'] == 'success') {
              this.toastr.success('Booking successful')
              this.BookingDetails = response['data']
              console.log(this.BookingDetails)
              console.log(this.BookingDetails['bookingId'])
              this.router.navigate(['customer/hotel-booking', { id: this.BookingDetails['bookingId'] }])
            } else {
              console.log(response['error'])
              this.toastr.error('Booking failed')
            }
          }
        })
        console.log("room booked")
      }
    }
  }


  validateDates(sDate: number, eDate: number) {
    this.isValidDate = true;
    if ((sDate == null || eDate == null) || (sDate == null && eDate == null)) {
      this.toastr.error('Start date and end date are required')
      this.isValidDate = false;
    }

    if ((sDate != null && eDate != null) && (eDate) < (sDate)) {
      this.toastr.error('End date should be greater then start date.')
      this.isValidDate = false;
    }
    return this.isValidDate;
  }
}


/**
 * roomdata  3   static
 *
 * booking data array ->
 * checkin<= date => checkout
 * result =   static -  no of rooms booking
 *
 * result <1
 * toastr
 *
 * checkin < =  date =>checkout date
*/


import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  url = 'http://35.153.50.100:3000/customer'

  constructor(
    private httpClient: HttpClient
  ) { }
  //============================CUSTOMER PROFILE===========================
  getProfileDetail() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/get-profile", httpOptions)
  }
  //=============================GET CUSTOMER OTHER INFO========================
  getOtherProfileDetail() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/get-other-info", httpOptions)
  }
  getOtherDetail() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    return this.httpClient.get(this.url + "/get-other-details", httpOptions)
  }
  //================================UPDATE PROFILE==============================
  updateProfile(FirstName: string, LastName: string, Contact: string, Address: string, Email: string, pin: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      custFirstName: FirstName,
      custLastName: LastName,
      custMobileNo: Contact,
      custAddress: Address,
      custEmail: Email,
      cityPin: pin
    }
    return this.httpClient.put(this.url + "/update-profile", body, httpOptions)
  }
  //================================UPLOAD OTHER INFO=========================================
  updateOtherInfo(status: string, age: number, date: Date, gender: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      custMaritalStatus: status,
      custAge: age,
      custBirthDate: date,
      custGender: gender
    }
    return this.httpClient.put(this.url + "/update-other-details", body, httpOptions)
  }
  //=================================DELETE ACCOUNT ==========================================
  deletecustAccount() {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {

    }
    return this.httpClient.post(this.url + "/delete-profile", body, httpOptions)
  }

  CustGuesturl = 'http://35.153.50.100:3000/customer/guest'

  //================================LOAD GUEST===============================================
  getGuests() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.CustGuesturl, httpOptions)
  }

  //=====================================UPDATE GUEST=========================================
  updateGuest(id, name: string, gender: string, age: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      name: name,
      gender: gender,
      age: age
    }

    return this.httpClient.put(this.CustGuesturl + "/update" + id, body, httpOptions)
  }

  //=====================================ADD GUEST===========================================
  insertGuest(name: string, gender: string, age: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      name: name,
      gender: gender,
      age: age

    }

    return this.httpClient.post(this.CustGuesturl + "/insert", body, httpOptions)
  }

  //===================================GET GUEST DETAILS===================================
  getGuestDetails(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.CustGuesturl + "/" + id, httpOptions)
  }

  //================================DELETE SPECIFIC GUEST================================
  deleteGuest(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.httpClient.delete(this.CustGuesturl + "/id" + id, httpOptions)
  }
  //==============================DELETE ALL GUEST======================================
  deleteAllGuest() {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.httpClient.delete(this.CustGuesturl + "/all", httpOptions)
  }
  //================================CHANGE PASSWORD========================================
  changePasssword(oldPassword: string, newPassword: string) {
    const httpOptions =
    {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    const body = {
      oldPassword: oldPassword,
      newPassword: newPassword
    }
    return this.httpClient.post(this.url + "/reset-password", body, httpOptions)

  }
  //=================================UPLOAD IMAGE===========================================
  uploadImage(file) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = new FormData()
    body.append('image', file)

    return this.httpClient.post(this.url + `/upload-profile-pic/`, body, httpOptions)
  }
}


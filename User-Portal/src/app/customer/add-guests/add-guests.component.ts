import { CustomerService } from './../customer.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-guests',
  templateUrl: './add-guests.component.html',
  styleUrls: ['./add-guests.component.css']
})
export class AddGuestsComponent implements OnInit {


  name = ''
  gender = ''
  age = 0
  id

  guest = null
  //custGuestName,custGuestAge,custGuestGender
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.queryParams['id']
    console.log(this.id)

    if (this.id) {
      this.customerService
        .getGuestDetails(this.id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const guests = response['data']
            if (guests.length > 0) {
              this.guest = guests[0]
              this.name = this.guest['custGuestName']
              this.gender = this.guest['custGuestGender']
              this.age = this.guest['custGuestAge']
            }
          }
        })
    }
  }


  onUpdate() {
    if (this.gender == '' || this.age == null || this.name == '') {
      this.toastr.error("All fields are mandatory")
    }
    else if (this.guest) {
      // edit
      this.customerService
        .updateGuest(this.guest['custGuestId'], this.name, this.gender, this.age)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/customer/guests'])
            this.toastr.success('Guest Updated successfully...!!')
          }
        })
    } else {
      // insert
      this.customerService
        .insertGuest(this.name, this.gender, this.age)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/customer/guests'])
            this.toastr.success('Guest Added successfully...!!')
          }
        })
    }

  }

}

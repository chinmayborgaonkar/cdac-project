import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustRestaurantService } from '../cust-restaurant.service';

@Component({
  selector: 'app-restaurant-booking',
  templateUrl: './restaurant-booking.component.html',
  styleUrls: ['./restaurant-booking.component.css']
})
export class RestaurantBookingComponent implements OnInit {
  public sub: any;
  name = ''
  date = null
  status = ''
  noOfTables = 0
  Detail = []
  id = 0
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private custRestaurantService: CustRestaurantService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.sub = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.sub + " vrushali")
    this.custRestaurantService.getRestaurantBooking(this.sub).subscribe(response => {
      if (response['status'] == 'success') {
        this.Detail = response['data']
        this.name = this.Detail[0]['custBookedRest']
        this.date = this.Detail[0]['custRestBookingDate'] + 1
        this.status = this.Detail[0]['custBookingStatus']
        this.noOfTables = this.Detail[0]['custNoOfTables']
        this.id = this.Detail[0]['restBookingId']
        console.log(this.Detail[0]['custRestBookingDate'])
      } else {
        console.log(response['error'])
      }
    })
  }
  giveRating() {
    this.router.navigate(['customer/rest-rating', { id: this.id }])
  }

}

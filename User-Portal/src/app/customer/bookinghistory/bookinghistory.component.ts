import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustHotelService } from '../cust-hotel.service';
import { CustRestaurantService } from '../cust-restaurant.service';

@Component({
  selector: 'app-bookinghistory',
  templateUrl: './bookinghistory.component.html',
  styleUrls: ['./bookinghistory.component.css']
})
export class BookinghistoryComponent implements OnInit {
  bookingsarray = []
  name = ''
  bookingsHotel
  nameHotel
  constructor(
    private custRestaurantService: CustRestaurantService,
    private custHotelService: CustHotelService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadAllBookingDetails()
    this.loadAllHotelBookingDetails()
  }

  loadAllBookingDetails() {
    this.custRestaurantService
      .getAllBooking()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.bookingsarray = response['data']
          this.name = this.bookingsarray[0]['custBookedRest']
          console.log(this.bookingsarray)
          console.log(this.name)
        } else {
          console.log(response['error'])
        }
      })
  }
  cancelBooking(book) {
    console.log(book['restBookingId'])
    this.custRestaurantService
      .cancelBooking(book['restBookingId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Booking cancelled successfully...!!')
          this.loadAllBookingDetails()
        } else {
          console.log(response['error'])
        }
      })
  }


  loadAllHotelBookingDetails() {
    this.custHotelService
      .getAllBooking()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.bookingsHotel = response['data']
          console.log(this.bookingsHotel)
        } else {
          console.log(response['error'])
        }
      })
  }
  cancelHotelBooking(book) {
    console.log(book['hotelBookingId'])
    this.custHotelService
      .cancelBooking(book['hotelBookingId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Booking cancelled successfully...!!')
          this.loadAllHotelBookingDetails()
        } else {
          console.log(response['error'])
        }
      })
  }
}

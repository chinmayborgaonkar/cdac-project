
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustRestaurantService } from '../cust-restaurant.service';
import { CustomerService } from '../customer.service';


@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.css']
})
export class RestaurantDetailsComponent implements OnInit {
  Details = []
  picDetails = []
  menuDetails = []
  BookingDetails = []
  restid
  restPic = null
  action = 0
  cityPin = 0
  NoOfTables = 1
  dateBook
  guests = []
  guestArray = []
  guestCounter = 0
  bookingsarray
  BookNow = 0
  result = 0
  tablesRemaining = 0

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private customerService: CustomerService,
    private custRestaurantService: CustRestaurantService,
    private activatedRoute: ActivatedRoute
  ) { }


  ngOnInit(): void {
    this.restid = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.restid)
    this.loadRestData()
    this.loadGuests()
    this.loadAllBookingRDetails()
  }


  loadAllBookingRDetails() {
    this.custRestaurantService
      .getRestBookingDetails(this.restid)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.bookingsarray = response['data']
          console.log(this.bookingsarray)
        } else {
          console.log(response['error'])
        }
      })
  }

  loadOninit() {
    this.tablesRemaining = 0
    this.result = 0
    this.bookingsarray
    const nooftables = this.Details[0].restNoOfTables //10
    //const nooftables = 10
    console.log(this.dateBook)
    for (let book of this.bookingsarray) {
      const d = book.custRestBookingDate.split('T')[0];

      if (this.dateBook == d) {
        this.result += book.custNoOfTables
        console.log("total tble" + this.result)
      }
    }
    if (this.result > nooftables)
      this.toastr.error("tables not available");
    else {
      this.tablesRemaining = nooftables - this.result
      console.log("remaining " + this.tablesRemaining)
    }
  }

  goToGuests() {
    this.router.navigate(['/customer/guests'])
  }

  loadGuests() {
    this.customerService
      .getGuests()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.guestArray = response['data']
          console.log(this.guestArray)
          if (this.guestArray.length > 0) {
            this.guestCounter = 1;
          }
          else {
            this.guestCounter = 0
          }

        } else {
          console.log(response['error'])
        }
      })
  }


  toggleEditable(event: any, item: any) {
    if (event.target.checked) {
      this.guests.push({
        custRestGuestsName: item.custGuestName, custRestGuestsAge: item.custGuestAge, custRestGuestsGender: item.custGuestGender
      })
    }
    else {
      const index = this.guests.findIndex(b => b.custRestGuestsName == item.custGuestName);
      console.log(index)
      if (index != -1) {
        this.guests.splice(index, 1);
      }
    }
    console.log(this.guests)
  }



  loadRestData() {
    console.log(this.restid)
    this.custRestaurantService.getRestDetails(this.restid).subscribe(response => {
      if (response['status'] == 'success') {
        console.log(response['data']);
        this.Details = response['data']['restDetails']
        // this.picDetails = response['data']['RestPics']
        this.menuDetails = response['data']['menudata']
        this.cityPin = this.Details[0]['cityPin']
        if (sessionStorage.length > 0)
          this.BookNow = 1
      } else {
        console.log(response['error'])
      }
    })
  }
  goBackToPreviousPage() {
    this.router.navigate(['/'])
  }
  BookTable() {
    if (this.BookNow == 1) {
      this.action = 1
    }
    else {
      this.toastr.success("Please Login to book table")
    }
  }
  showmenu() {
    this.action = 2
  }
  addBookings() {
    if (this.NoOfTables > this.tablesRemaining) {
      if (this.tablesRemaining > 0)
        this.toastr.error("cannot book tables more than " + this.tablesRemaining);
      else {
        this.toastr.error("no tables available");
      }
    }
    else {
      if (this.cityPin == null || this.NoOfTables == null || this.dateBook == null || this.restid == null) {
        this.toastr.error('All fields are mandatory')
      } else {
        this.custRestaurantService.addBookings(this.cityPin, this.NoOfTables, this.dateBook, this.restid, this.guests).subscribe(response => {
          if (response['status'] == 'success') {
            this.toastr.success('Booking successful')
            this.BookingDetails = response['data']
            console.log(this.BookingDetails)
            console.log(this.BookingDetails['bookingId'])
            this.router.navigate(['customer/restaurant-booking', { id: this.BookingDetails['bookingId'] }])
          } else {
            console.log(response['error'])
            this.toastr.error('Booking failed')
          }
        })
        console.log("table booked")
      }
    }

  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustRestaurantService } from '../cust-restaurant.service';

@Component({
  selector: 'app-rest-rating',
  templateUrl: './rest-rating.component.html',
  styleUrls: ['./rest-rating.component.css']
})
export class RestRatingComponent implements OnInit {
  rating = 5
  public no: any;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private custRestaurantService: CustRestaurantService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.no = this.activatedRoute.snapshot.paramMap.get('id');
  }

  updateRating() {
    console.log(this.no)
    console.log(this.rating)
    if (this.rating != null && (this.rating > 0 && this.rating < 6)) {
      this.custRestaurantService.updateRating(this.no, this.rating).subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Thank you for rating us')
          this.router.navigate(['/dashboard'])
        }
        else {
          this.toastr.error('Update Rating failed')
        }
      })
    }
    else {
      this.toastr.error('Rating shound be between 1 & 5')
    }
  }
}

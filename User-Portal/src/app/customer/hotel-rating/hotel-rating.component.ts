import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustHotelService } from '../cust-hotel.service';
import { CustRestaurantService } from '../cust-restaurant.service';

@Component({
  selector: 'app-hotel-rating',
  templateUrl: './hotel-rating.component.html',
  styleUrls: ['./hotel-rating.component.css']
})
export class HotelRatingComponent implements OnInit {

  rating = 5
  public no: any;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private custHotelService: CustHotelService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.no = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.no)
  }

  updateRating() {
    console.log(this.no)
    console.log(this.rating)
    if (this.rating != null && (this.rating > 0 && this.rating < 6)) {
      this.custHotelService.updateRating(this.no, this.rating).subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Thank you for rating us')
          this.router.navigate(['/dashboard'])
        }
        else {
          this.toastr.error('Update Rating failed')
        }
      })
    }
    else {
      this.toastr.error('Rating shound be between 1 & 5')
    }
  }
}

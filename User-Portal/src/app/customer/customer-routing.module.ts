import { OtherinfoComponent } from './otherinfo/otherinfo.component';
import { RestRatingComponent } from './rest-rating/rest-rating.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AuthService } from './../auth/auth.service';
import { ProfileComponent } from './profile/profile.component';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';
import { GuestsComponent } from './guests/guests.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { RestaurantBookingComponent } from './restaurant-booking/restaurant-booking.component';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { AddGuestsComponent } from './add-guests/add-guests.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { BookinghistoryComponent } from './bookinghistory/bookinghistory.component';
import { HotelRatingComponent } from './hotel-rating/hotel-rating.component';

const routes: Routes = [
  { path: 'guests', component: GuestsComponent, canActivate: [AuthService] },
  { path: 'hotel-booking', component: HotelBookingComponent, canActivate: [AuthService] },
  { path: 'hotel-details', component: HotelDetailsComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthService] },
  { path: 'restaurant-booking', component: RestaurantBookingComponent, canActivate: [AuthService] },
  { path: 'restaurant-details', component: RestaurantDetailsComponent },
  { path: 'profile-edit', component: EditProfileComponent, canActivate: [AuthService] },
  { path: 'add-guest', component: AddGuestsComponent, canActivate: [AuthService] },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthService] },
  { path: 'upload-profile-image', component: UploadImageComponent, canActivate: [AuthService] },
  { path: 'rest-rating', component: RestRatingComponent, canActivate: [AuthService] },
  { path: 'hotel-rating', component: HotelRatingComponent, canActivate: [AuthService] },
  { path: 'rest-history', component: BookinghistoryComponent, canActivate: [AuthService] },
  { path: 'other-details', component: OtherinfoComponent, canActivate: [AuthService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }

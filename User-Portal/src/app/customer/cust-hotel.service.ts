import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CustHotelService {
  urlCustomer = "http://35.153.50.100:3000/customer/hotel"
  bookingurl = 'http://35.153.50.100:3000/customer/hotel/booking'
  bookinghotel = 'http://35.153.50.100:4000/hotel/booking'

  constructor(private router: Router,
    private httpClient: HttpClient) { }

  getHotelDetails(Id) {
    const body = {
      hotelId: Id
    }
    return this.httpClient.post(this.urlCustomer + "/Id", body)
  }

  getHotelBookingDetails(Id) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      hotelId: Id
    }
    return this.httpClient.get(this.bookinghotel + '/search-by-id/'+Id, httpOptions)
  }

 

  

  addBooking(hotelId, custBookedHotel, custCheckInDate, custCheckOutDate, custNoOfGuests, custNoOfRooms, cityPin, guests) {
    const body = {
      hotelId: hotelId,
      custBookedHotel: custBookedHotel,
      custCheckInDate: custCheckInDate,
      custCheckOutDate: custCheckOutDate,
      custNoOfGuests: custNoOfGuests,
      custNoOfRooms: custNoOfRooms,
      cityPin: cityPin,
      guests: guests
    }

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.post(this.urlCustomer + "/booking/", body, httpOptions)
  }

  getHotelBooking(sub) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    console.log(sub)

    return this.httpClient.get(this.bookingurl + '/get-booking-details/' + sub, httpOptions)
  }

  updateRating(bookingId, rate) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      rate: rate,
      bookingId: bookingId
    }
    console.log(bookingId)
    return this.httpClient.put(this.bookingurl + '/update-rating', body, httpOptions)
  }

  getAllBooking() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.bookingurl + '/get-booking-details-by-id', httpOptions)
  }

  cancelBooking(id: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.httpClient.get(this.bookingurl + "/cancel-booking/" + id, httpOptions)
  }
}

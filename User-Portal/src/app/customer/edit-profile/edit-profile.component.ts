import { CustomerService } from './../customer.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  FirstName = ''
  LastName = ''
  Contact = ''
  Address = ''
  Email = ''
  pin = 1


  constructor(
    private toastr: ToastrService,
    private customerService: CustomerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.customerService.getProfileDetail().subscribe(response => {
      if (response['status'] == 'success') {
        const details = response['data']
        this.FirstName = details[0]['custFirstName']
        this.LastName = details[0]['custLastName']
        this.Address = details[0]['custAddress']
        this.Contact = details[0]['custMobileNo']
        this.pin = details[0]['cityPin']
        this.Email = details[0]['custEmail']

      }
    })

  }

  onUpadte() {
    if (this.FirstName == '' ||
      this.LastName == '' ||
      this.Contact == null ||
      this.Address == '' ||
      this.Email == '' ||
      this.pin == null) {
      this.toastr.error("All fields are mandatory")
    } else {
      this.customerService.updateProfile(this.FirstName, this.LastName, this.Contact, this.Address, this.Email, this.pin).subscribe
        (response => {
          if (response['status'] == 'success') {
            this.toastr.success('profile updated successfully...!!!!')
            this.router.navigate(['/customer/profile'])
          }
          else {
            this.toastr.error('profile updation failed...!!!!')
            console.log(response['error'])
          }
        })
    }
  }


}

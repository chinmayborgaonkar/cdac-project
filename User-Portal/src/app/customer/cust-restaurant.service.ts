import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustRestaurantService {
  url = 'http://35.153.50.100:3000/customer/restaurant'
  bookingurl = 'http://35.153.50.100:3000/customer/restaurant/booking'
  bookingrest = 'http://35.153.50.100:4000/restaurant/booking'

  constructor(
    private httpClient: HttpClient
  ) { }

  getRestBookingDetails(Id) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      restId: Id
    }

    return this.httpClient.get(this.bookingrest + '/search-by-id/'+Id, httpOptions)
  }
  getRestDetails(id) {

    console.log(id)
    return this.httpClient.get(this.url + '/Id/' + id)
  }

  addBookings(cityPin: number, NoOfTables: number, date: Date, restId,guests) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      cityPin: cityPin,
      custNoOfTables: NoOfTables,
      custRestBookingDate: date,
      id: restId,
      guests:guests
    }

    return this.httpClient.post(this.bookingurl + '/add-booking', body, httpOptions)
  }

  getRestaurantBooking(sub: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    console.log(sub)

    return this.httpClient.get(this.bookingurl + '/get-booking-details/' + sub, httpOptions)
  }
  getAllBooking() {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.bookingurl + '/get-booking-details-by-id', httpOptions)
  }


  updateRating(no: number, rating: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      no: no,
      rating: rating
    }
    console.log(no)
    return this.httpClient.put(this.bookingurl + '/update-rating', body, httpOptions)
  }

  cancelBooking(id: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.httpClient.get(this.bookingurl + "/cancel-booking/" + id, httpOptions)
  }
}

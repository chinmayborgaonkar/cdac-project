import { CustomerService } from './../customer.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.css']
})
export class GuestsComponent implements OnInit {

  guests = []

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private customerService: CustomerService) { }

  ngOnInit(): void {
    this.loadGuests()
  }

  loadGuests() {
    this.customerService
      .getGuests()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.guests = response['data']
          console.log(this.guests)
        } else {
          console.log(response['error'])
        }
      })
  }

  deleteAllGuest() {
    this.customerService
      .deleteAllGuest().subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('All Guest deleted successfully...!!')
          this.loadGuests()
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(guest) {
    this.router.navigate(['/customer/add-guest'], { queryParams: { id: guest['custGuestId'] } })
  }


  addGuest() {
    this.router.navigate(['/customer/add-guest'])

  }
  onDelete(guest) {
    this.customerService
      .deleteGuest(guest['custGuestId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Guest deleted successfully...!!')
          this.loadGuests()

        } else {
          console.log(response['error'])
        }
      })
  }
}

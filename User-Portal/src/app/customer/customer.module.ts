import { CustRestaurantService } from './cust-restaurant.service';
import { CustHotelService } from './cust-hotel.service';
import { CustomerService } from './customer.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from './customer-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { GuestsComponent } from './guests/guests.component';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { RestaurantBookingComponent } from './restaurant-booking/restaurant-booking.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { FormsModule } from '@angular/forms';
import { AddGuestsComponent } from './add-guests/add-guests.component';
import { HttpClientModule } from '@angular/common/http';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { RestRatingComponent } from './rest-rating/rest-rating.component';
import { BookinghistoryComponent } from './bookinghistory/bookinghistory.component';
import { HotelRatingComponent } from './hotel-rating/hotel-rating.component';
import { OtherinfoComponent } from './otherinfo/otherinfo.component';


@NgModule({
  declarations: [
    ProfileComponent,
    GuestsComponent,
    HotelDetailsComponent,
    HotelBookingComponent,
    RestaurantDetailsComponent,
    RestaurantBookingComponent,
    EditProfileComponent,
    AddGuestsComponent,
    ChangePasswordComponent,
    UploadImageComponent,
    RestRatingComponent,
    BookinghistoryComponent,
    HotelRatingComponent,
    OtherinfoComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    CustomerService,
    CustHotelService,
    CustRestaurantService
  ]
})
export class CustomerModule { }

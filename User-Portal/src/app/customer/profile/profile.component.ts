import { ChangePasswordComponent } from './../change-password/change-password.component';
import { CustomerService } from './../customer.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  Details = []
  otherDetails = []

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    this.loadProfileData()
    this.getOtherDetails()
  }

  loadProfileData() {
    this.customerService.getProfileDetail().subscribe(response => {
      if (response['status'] == 'success') {
        this.Details = response['data']
      } else {
        console.log(response['error'])
      }
    })
  }

  getOtherDetails() {
    this.customerService.getOtherProfileDetail().subscribe(response => {
      if (response['status'] == 'success') {
        this.otherDetails = response['data']
        console.log(this.otherDetails[0]['custProfileImage'])
      } else {
        console.log(response['error'])
      }
    })
  }

  onEdit() {
    this.router.navigate(['/customer/profile-edit'])
  }
  changePassword() {
    this.router.navigate(['/customer/change-password'])
  }
  guestInfo(){
    this.router.navigate(['/customer/guests'])
  }

  deleteAccount() {
    this.customerService.deletecustAccount().subscribe(response => {
      if (response['status'] == 'success') {
        this.toastr.success('Account temporarily deleted')
        this.router.navigate(['/'])
      }
      else {
        this.toastr.error('Account deletion failed')
        console.log(response['error'])
      }
    })
  }
  uploadImage() {
    this.router.navigate(['/customer/upload-profile-image'])
  }
  getallBookings() {
    this.router.navigate(['/customer/rest-history'])
  }
  updateOtherInfo() {
    this.router.navigate(['/customer/other-details'])
  }
}

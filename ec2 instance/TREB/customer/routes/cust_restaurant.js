const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const fs = require('fs')
// Multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: 'images/' })

const router = express.Router()

//======================== GET =========================

//======================== GET All Restaurant ==========================
router.get('/All', (request, response) => {

    const statement = `select  restName,restMobileNo,restEmail,restAddressArea,restAddressStreet,restAddressLandmark,cityPin,
                      restDescription,restOpenTime,restCloseTime,restNoOfTables
                      from restaurantInfo `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== Get restaurant by Id =========================
router.get('/image/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../../images/' + filename)
    response.send(file)
})

router.get('/Id/:id', (request, response) => {
    const { id } = request.params

    const statement = `select restName,restMobileNo ,restEmail,restAddressArea,restAddressStreet,restAddressLandmark,
                       cityPin,restDescription,restOpenTime,restCloseTime,restNoOfTables
                       from restaurantInfo where restaurantId='${id}'  `

    db.query(statement, (error, data) => {
        const dbResult = {
            restDetails: data
        }

        const picStatement = `select restPic from restPicDetails where restaurantId='${id}'`

        db.query(picStatement, (error, dataPics) => {
            dbResult.RestPics = dataPics

            const menuStatement = `select foodItemName,foodItemPrice from restMenuDetails where restaurantId='${id}' AND foodItemAvailable=1`

            db.query(menuStatement, (error, menuData) => {
                dbResult.menudata = menuData
                response.send(utils.createResult(error, dbResult))
            })
        })
    })
})

//======================== POST =========================

//======================== Get restaurant list by location =========================
router.post('/city', (request, response) => {
    const { city } = request.body

    const statement = `select *
                       from restaurantInfo r inner join pinInfo pin
                       ON r.cityPin = pin.cityPin
                       where pin.city='${city}'`

    db.query(statement, (error, data) => {
        // const dbResult = {
        //     restDetails: data
        // }
        // const picStatement = `select restPic from restPicDetails where restaurantId='${data[0].restaurantId}'`

        // db.query(picStatement, (error, dataPics) => {
        //     dbResult.RestPics = dataPics

        response.send(utils.createResult(error, data))
        // })
    })
})

//======================== Get Restaurant list by name =========================
router.post('/name', (request, response) => {
    const { hotelName } = request.body

    const statement = `select * from restaurantInfo where restName='${hotelName}'`

    db.query(statement, (error, data) => {
        // const dbResult = {
        //     restDetails: data
        // }
        // const picStatement = `select restPic from restPicDetails where restaurantId='${data[0].restaurantId}'`

        // db.query(picStatement, (error, dataPics) => {

        //     dbResult.RestPics = dataPics

        response.send(utils.createResult(error, data))
        // })
    })
})

//======================== Get Restaurant food items =========================
router.post('/menuItems', (request, response) => {
    const { id } = request.body

    const statement = `select foodItemId,foodItemName,foodItemPrice from restMenuDetails where restaurantId='${id}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== Get Restaurant food items =========================
router.post('/menuItemsName', (request, response) => {
    const { foodItem } = request.body

    const statement = `select  * from restMenuDetails f inner join restaurantInfo r  where foodItemName='${foodItem}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//======================== Get Restaurant by booking id =========================
router.post('/BookingId', (request, response) => {
    const { RestBookingId } = request.body

    const statement = `select r.restaurantId,r.restaurantId, r.restName,r.restMobileNo ,r.restEmail,r.restAddressArea,r.restAddressStreet,
                       r.restAddressLandmark,r.cityPin,r.restDescription,r.restOpenTime,r.restCloseTime, b.custNoOfTables
                      from restaurantInfo r inner JOIN custRestBookingInfo b
                      ON r.restaurantId=b.restaurantId
                      where restBookingId=${RestBookingId} AND customerId = ${request.userId}`

    db.query(statement, (error, data) => {

        const picStatement = `select restPic from restPicDetails where restaurantId='${data[0].restaurantId}'`

        db.query(picStatement, (error, dataPics) => {

            const dbResult = {
                restDetails: data,
                restPics: dataPics
            }
            response.send(utils.createResult(error, dbResult))
        })
    })
})
//===================PUT==================================

//================Update Customer other info========================
router.put("/update-customer", (request, response) => {
    const { custMaritalStatus,custAge, custBirthDate, custGender } = request.body
 
    const statement = `update customerOtherDetails set custMaritalStatus='${custMaritalStatus}', custAge=${custAge}, custBirthDate='${custBirthDate}',custGender='${custGender}' where customerId=${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
module.exports = router
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

//========================= GET =========================

//======================== Get details by BookingStatus =========================
router.post('/status', (request, response) => {
        const { bookingStatus } = request.body

        let statement = '';

        if (bookingStatus == "Completed" || bookingStatus == "Cancelled") {
            statement = `select * from custRestBookingInfo where custBookingStatus='${bookingStatus}' AND customerId = ${request.userId}`

            db.query(statement, (error, data) => {
                const result = {
                    "data": data,
                    "booking Status": bookingStatus
                }
                response.send(utils.createResult(error, result));
            })
        } else {
            statement = ` select * from custRestBookingInfo
                      where (custBookingStatus="Completed") 
                      AND ( CURDATE() = custRestBookingDate ) AND customerId = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    }
})
//=========================Get Booking Details By Customer Id=====================
router.get('/get-booking-details-by-id', (request, response) => {

    const statement = `select * from custRestBookingInfo where customerId='${request.userId}' And custBookingStatus='Completed' `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//=========================GET BOOKING DETAILS==============================
router.get('/get-booking-details/:sub', (request, response) => {
    const { sub } = request.params

    const statement = `select * from custRestBookingInfo where restBookingId='${sub}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= POST =========================

//========================= Add Restaurant Booking =========================

router.post('/add-booking1', (request, response) => {
    const { custNoOfTables, cityPin, id, custRestBookingDate } = request.body
    console.log(request.userId);
    const statement = `select * from restaurantInfo where restaurantId=${id}`

    db.query(statement, (error, data) => {
        console.log(data)
        console.log(custNoOfTables + "  " + cityPin + id + " " + custRestBookingDate + " ")
        const custstatement = `insert into custRestBookingInfo (customerId,restaurantId,custBookedRest,custRestBookingDate,custBookingStatus,custNoOfTables,cityPin) values 
               (${request.userId},${data[0]['restaurantId']},'${data[0]['restName']}','${custRestBookingDate}','Completed',${custNoOfTables},${cityPin})`
        console.log(custstatement);

        db.query(custstatement, (error, data1) => {
            response.send(utils.createResult(error, data1))
        })
    })
})


router.post('/add-booking', (request, response) => {
    const { custNoOfTables, cityPin, id, custRestBookingDate, guests } = request.body;
    const statement = `select * from restaurantInfo where restaurantId=${id}`
    db.query(statement, (error, data) => {

        const custstatement = `insert into custRestBookingInfo (customerId,restaurantId,custBookedRest,custRestBookingDate,custBookingStatus,custNoOfTables,cityPin) values  (${request.userId},${data[0]['restaurantId']},'${data[0]['restName']}','${custRestBookingDate}','Completed',${custNoOfTables},${cityPin})`;

        db.query(custstatement, (error, data1) => {
            const restBookingId = data1['insertId']
            console.log(restBookingId)
    
            let statementGuestDetails = `insert into custRestGuestsList (restBookingId,custRestGuestsName,custRestGuestsAge,custRestGuestsGender) values `
    
            for (let index = 0; index < guests.length; index++) {
                const guest = guests[index];
    
                if (index > 0) {
                    statementGuestDetails += ', '
                }
                statementGuestDetails += `(${restBookingId}, '${guest['custRestGuestsName']}', '${guest['custRestGuestsAge']}','${guest['custRestGuestsGender']}')`
            }
    
            db.query(statementGuestDetails, (error, data) => {
    
                const data1 = {
                    msg: 'guests added',
                    bookingId: restBookingId
                }
                response.send(utils.createSuccess(data1))
            })
        })
    })
})

//========================= PUT =========================

//========================= Cancel Restaurant Booking =========================
router.get('/cancel-booking/:id', (request, response) => {
    const { id } = request.params

    const statement = `update custRestBookingInfo set custBookingStatus="Cancelled"
                       where restBookingId=${id} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Update Rating =========================
router.put('/update-rating', (request, response) => {
    const { no, rating } = request.body

    const statement = `update custRestBookingInfo set custRestRating=${rating} where restBookingId=${no}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
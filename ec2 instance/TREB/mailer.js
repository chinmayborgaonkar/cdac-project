const nodemailer = require('nodemailer')
const config = require('./config')

// Send Mail
function sendEmail(Email, subject, body, callback) {
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: config.emailId,
            pass: config.emailPassword
        }
    })

    const options = {
        from: config.emailId,
        to: Email,
        subject: subject,
        html: body
    }

    transport.sendMail(options, callback)
}

module.exports = {
    sendEmail: sendEmail
}
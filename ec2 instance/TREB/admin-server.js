const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
const db = require('./db')
const utils = require('./utils')

// Morgan: for logging
const morgan = require('morgan')

// Routers
const hotelRouter = require('./hotel/routes/hotel')
const restaurantRouter = require('./restaurant/routes/restaurant')
const hotelServicesRouter = require('./hotel/routes/hotel-services')
const hotelRoomRouter = require('./hotel/routes/room')
const restaurantFoodItemRouter = require('./restaurant/routes/food_items')
const restaurantTablesRouter = require('./restaurant/routes/table')
const hotelBookingRouter = require('./hotel/routes/hotel_booking')
const restaurantBookingsRouter = require('./restaurant/routes/rest_booking')
const adminPanelRouter = require('./admin-panel')
const cors = require('cors');

const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))
app.use(cors('*'))

// Add a middleware for getting the id from token
function getAdmin(request, response, next) {

    if (request.url == '/city-pin' ||
        request.url == '/hotel/signup' ||
        request.url == '/hotel/signin' ||
        request.url == '/hotel/signup-insert-services' ||
        request.url.startsWith('/hotel/activate') ||
        request.url.startsWith('/hotel/image/') ||
        request.url.startsWith('/hotel/forgot-password') ||
        request.url == '/restaurant/signup' ||
        request.url == '/restaurant/signin' ||
        request.url.startsWith('/restaurant/image/') ||
        request.url == '/restaurant/signup-Menu' ||
        request.url.startsWith('/restaurant/activate') ||
        request.url.startsWith('/restaurant/forgot-password') ||
        request.url == '/admin/get-customer' ||
        request.url == '/admin/get-hotel' ||
        request.url == '/admin/get-rest' ||
        request.url == '/admin/get-hotel-booking' ||
        request.url == '/admin/get-rest-booking') {
        next()
    } else {
        try {
            const token = request.headers['token']
            const data = jwt.verify(token, config.secret)
            request.userId = data['id']
            next()

        } catch (ex) {
            response.send({ status: 'error', error: 'protected api' })
        }
    }
}

app.use(getAdmin)
app.use(cors())

// Required to send the static files in the directory named images   
app.use(express.static('images/'))

// Admin Routes
app.use('/admin', adminPanelRouter)


// Hotel Routes
app.use('/hotel', hotelRouter)
app.use('/hotel/services', hotelServicesRouter)
app.use('/hotel/room', hotelRoomRouter)
app.use('/hotel/booking', hotelBookingRouter)

// Restaurant Routes
app.use('/restaurant', restaurantRouter)
app.use('/restaurant/food', restaurantFoodItemRouter)
app.use('/restaurant/table', restaurantTablesRouter)
app.use('/restaurant/booking', restaurantBookingsRouter)

// Default Route
app.get('/', (request, response) => {
    response.send('welcome to TREB application')
})

// City Pin Route
app.post('/city-pin', (request, response) => {
    const { cityPin, city } = request.body
    const statement = `insert into pinInfo (cityPin,city) values (${cityPin},'${city}')`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data));
    })
})

// Admin Server on Port 4000
app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port 4000')
})
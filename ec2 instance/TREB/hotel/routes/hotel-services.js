const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

//========================= GET =========================

//========================= Get all services for specific Hotel =========================
router.get('/get-services', (request, response) => {

    const statement = `select hotelId,hotelServiceId,hotelServiceName,hotelServiceAvailable from hotelServiceDetails 
                      where hotelId=${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get all available services of specific hotel =========================
router.get('/get-available-services', (request, response) => {

    const statement = `select hotelId,hotelServiceId,hotelServiceName,hotelServiceAvailable from hotelServiceDetails 
                      where hotelId=${request.userId} AND hotelServiceAvailable=1`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= POST =========================
//
//========================= ADD SINGLE SERVICE =========================

router.post("/add-service", (request, response) => {
    const { hotelServiceName } = request.body

    let statement = `insert into hotelServiceDetails (hotelId,hotelServiceName) values 
                         (${request.userId},'${hotelServiceName}')`


    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= PUT =========================
// 
//========================= Update available services status =========================
router.put('/update-services', (request, response) => {
    const { hotelServiceName, hotelServiceAvailable } = request.body

    const statement = `update hotelServiceDetails set hotelServiceAvailable=${hotelServiceAvailable} 
                     where( hotelId=${request.userId} AND hotelServiceName="${hotelServiceName}")`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



module.exports = router
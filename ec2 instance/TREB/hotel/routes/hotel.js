const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')
const crypto = require('crypto-js')
const mailer = require('../../mailer')
const uuid = require('uuid')

// Multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: __dirname + '/../../images/' })
const fs = require('fs')
const router = express.Router()

//========================= GET =========================

//========================= GET Profile =========================
router.get('/get-profile', (request, response) => {

    const statement = ` select hotelName, hotelAdminName, hotelMobileNo, hotelEmail, hotelAddressArea, hotelAddressStreet, hotelAddressLandmark, cityPin, hotelDescription, hotelNoOfRooms, hotelPassword from hotelInfo where hotelId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Hotel ACtivation =========================
router.get('/activate/:token', (request, response) => {
    const { token } = request.params

    const statement = `update hotelInfo set active = 1, activationToken = '' where activationToken = '${token}'`

    db.query(statement, (error, data) => {
        const body = `activation successful....!!!!!`
        response.send(body)
    })
})

//========================= Forgot Password =========================
router.get('/forgot-password', (request, response) => {
    const { email } = request.body

    const statement = `select hotelId, hotelName, hotelAdminName from hotelInfo where hotelEmail = '${email}'`

    db.query(statement, (error, hotels) => {

        if (error) {
            response.send(utils.createError(error))
        } else if (hotels.length == 0) {
            response.send(utils.createError('Hotel does not exists'))
        } else {
            const hotel = hotels[0]
            const otp = utils.generateOTP()

            const body = `Dear Hotel Admin, use the following otp to reset your account pasword.Your otp = ${otp}`

            mailer.sendEmail(email, 'Reset your password', body, (error, info) => {
                response.send(utils.createResult(error, {
                    otp: otp,
                    email: email
                }))
            })
        }
    })
})




//========================= POST =========================

//========================= Sign Up =========================
router.post('/signup', (request, response) => {
    const { hotelName, hotelAdminName, hotelMobileNo, hotelEmail, hotelAddressArea, hotelAddressStreet, hotelAddressLandmark, cityPin, hotelDescription, hotelNoOfRooms, hotelPassword } = request.body
    const encryptedHotelPassword = crypto.SHA256(hotelPassword)
    const activationTokenHotel = uuid.v4()
    const activationLink = `http://35.153.50.100:4000/hotel/activate/${activationTokenHotel}`

    const body = `
    <h1>welcome ${hotelAdminName}</h1>
    <p>please activate your account by visiting the following link</p>
    <p><a href="${activationLink}">activate</a></p>
    <p>admin,</p>
    <p>TREB.com</p>
    `

    const statement = `insert into hotelInfo (hotelName,hotelAdminName,hotelMobileNo ,hotelEmail,hotelAddressArea,hotelAddressStreet,
    hotelAddressLandmark,cityPin,hotelDescription,hotelNoOfRooms,hotelPassword,activationToken)
   values ('${hotelName}','${hotelAdminName}','${hotelMobileNo}','${hotelEmail}','${hotelAddressArea}','${hotelAddressStreet}',
   '${hotelAddressLandmark}', '${cityPin}','${hotelDescription}','${hotelNoOfRooms}',
   '${encryptedHotelPassword}','${activationTokenHotel}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
        const otherDetails = `insert into hotelPicDetails(hotelId) values(${data['insertId']})`

        db.query(otherDetails, (error, data1) => {
            mailer.sendEmail(hotelEmail, "welcome to TakeRestEatBest ", body, (error, info) => {
                response.send(utils.createResult(error, data1))
            })
        })
    })
})

//========================= Sign Up with Services =========================
router.post('/signup-insert-services', (request, response) => {
    const { hotelName, hotelAdminName, hotelMobileNo, hotelEmail, hotelAddressArea, hotelAddressStreet, hotelAddressLandmark, cityPin, hotelDescription, hotelNoOfRooms, hotelPassword, services } = request.body

    const encryptedHotelPassword = crypto.SHA256(hotelPassword)
    const activationTokenHotel = uuid.v4()
    const activationLink = `http://35.153.50.100:4000/hotel/activate/${activationTokenHotel}`

    const body = `
    <h1>welcome ${hotelAdminName}</h1>
    <p>please activate your account by visiting the following link</p>
    <p><a href="${activationLink}">activate</a></p>
    <p>admin,</p>
    <p>TREB.com</p>
    `

    const statement = `insert into hotelInfo (hotelName,hotelAdminName,hotelMobileNo ,hotelEmail,hotelAddressArea,hotelAddressStreet,
    hotelAddressLandmark,cityPin,hotelDescription,hotelNoOfRooms,hotelPassword,activationToken)
   values ('${hotelName}','${hotelAdminName}','${hotelMobileNo}','${hotelEmail}','${hotelAddressArea}','${hotelAddressStreet}',
   '${hotelAddressLandmark}', '${cityPin}','${hotelDescription}','${hotelNoOfRooms}',
   '${encryptedHotelPassword}','${activationTokenHotel}')`

    db.query(statement, (error, data) => {

        mailer.sendEmail(hotelEmail, "welcome to TakeRestEatBest ", body, (error, info) => {
            response.send(utils.createResult(error, data))
        })
        const hotelId = data['insertId']

        let statementservicesDetails = `insert into hotelServiceDetails (hotelId,hotelServiceName,hotelServiceAvailable) values `

        for (let index = 0; index < services.length; index++) {
            const service = services[index];

            if (index > 0) {
                statementservicesDetails += ', '
            }
            statementservicesDetails += `(${hotelId}, '${service['hotelServiceName']}', ${service['hotelServiceAvailable']})`
        }

        db.query(statementservicesDetails, (error, data) => {
            response.send(utils.createSuccess('services added'))
        })
    })
})

//========================= Sign In =========================
router.post('/signin', (request, response) => {
    const { email, password } = request.body

    const statement = `select hotelId , hotelName , hotelAdminName , active from hotelInfo where hotelEmail = '${email}' and hotelPassword = '${crypto.SHA256(password)}'`

    db.query(statement, (error, hotels) => {

        if (error) {
            response.send(utils.createError(error))
        } else if (hotels.length == 0) {
            response.send(utils.createError('Hotel does not exists'))
        } else {
            const hotel = hotels[0]
            if (hotel['active'] == 1) {
                const token = jwt.sign({ id: hotel['hotelId'] }, config.secret)

                response.send(utils.createResult(error, {
                    hotelName: hotel['hotelName'],
                    hotelAdminName: hotel['hotelAdminName'],
                    token: token,
                    role: "hotel"
                }))
            } else {
                response.send(utils.createError('Dear Hotel Admin, Your account is not activated. Please activate your account for signing in to our application.'))
            }
        }
    })
})

//========================= Upload Hotel Images =========================
router.post('/upload-hotel-pics', upload.single('image'), (request, response) => {
    const fileName = request.file.filename

    const statement = `update hotelPicDetails set hotelPic='${fileName}' where hotelId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/image/:filename', (request, response) => {
    const { filename } = request.params
    const file = fs.readFileSync(__dirname + '/../../images/' + filename)
    response.send(file)
})


router.get('/get-other-info', (request, response) => {

    const statement = `select hotelPic from hotelPicDetails where hotelId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})




//======================== Reset Password =========================
router.post('/reset-password', (request, response) => {
    const { oldPassword, newPassword } = request.body

    const statement = `select hotelPassword from hotelInfo where hotelPassword= '${crypto.SHA256(oldPassword)}' AND hotelId=${request.userId} `

    db.query(statement, (error, data) => {

        if (error) {
            response.send("password does not match")
        } else {
            const ChangePass = `update hotelInfo set hotelPassword='${crypto.SHA256(newPassword)}' where hotelId=${request.userId}`

            db.query(ChangePass, (error, dataPass) => {
                response.send(utils.createResult(error, dataPass))
            })
        }
    })
});

//========================= PUT =========================

//========================= Update Profile =========================
router.put('/update-profile', (request, response) => {
    const { id } = request.params
    const { hotelName, hotelAdminName, hotelMobileNo, hotelEmail, hotelAddressArea, hotelAddressStreet, hotelAddressLandmark, cityPin, hotelDescription, hotelNoOfRooms } = request.body

    const statement = `update hotelInfo set 
        hotelName = '${hotelName}',
        hotelAdminName = '${hotelAdminName}',
        hotelMobileNo = '${hotelMobileNo}',
        hotelEmail = '${hotelEmail}',
        hotelAddressArea = '${hotelAddressArea}',
        hotelAddressStreet = '${hotelAddressStreet}',
        hotelAddressLandmark = '${hotelAddressLandmark}',
        cityPin = '${cityPin}',
        hotelDescription = '${hotelDescription}',
        hotelNoOfRooms = '${hotelNoOfRooms}'
        where hotelId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= DELETE =========================

//========================= Delete Profile =========================
router.post('/delete-profile', (request, response) => {

    const statement = `update hotelInfo set active = 0 where hotelId = ${request.userId} `
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
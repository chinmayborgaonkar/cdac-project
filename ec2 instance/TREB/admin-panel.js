const express = require('express')
const db = require('./db')
const config = require('./config')
const utils = require('./utils')
const jwt = require('jsonwebtoken')
const crypto = require('crypto-js')
const mailer = require('./mailer')
const uuid = require('uuid')

// Multer: used for uploading document
const multer = require('multer')

const router = express.Router()

//========================= GET =========================

//========================= GET Customers =========================
router.get('/get-customer', (request, response) => {

    const statement = ` select customerId,custFirstName,custLastName,custAddress,custMobileNo,custEmail,cityPin,
                        custLoggedInStatus from customerInfo `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Hotels =========================
router.get('/get-hotel', (request, response) => {

    const statement = ` select hotelName , hotelAdminName, hotelMobileNo , hotelEmail,hotelAddressArea,hotelAddressStreet,
                       hotelAddressLandmark,cityPin,hotelDescription,hotelNoOfRooms,hotelLoggedInStatus from hotelInfo `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Restaurants =========================
router.get('/get-rest', (request, response) => {

    const statement = ` select restName ,restAdminName,restMobileNo,restEmail,restAddressArea,restAddressStreet,restAddressLandmark,
                        cityPin,restDescription,restOpenTime,restCloseTime,restNoOfTables,restLoggedInStatus from restaurantInfo `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Restaurant-booking =========================
router.get('/get-rest-booking', (request, response) => {

    const statement = ` select c.custFirstName,c.custLastName,c.custEmail,b.custBookedRest,b.custRestBookingDate,
                        b.custRestRating,b.custRestBookedOn,b.custBookingStatus,b.custNoOfTables,b.cityPin
                        from custRestBookingInfo b inner join customerInfo c
                        ON c.customerId=b.customerId  `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Hotel-booking =========================
router.get('/get-hotel-booking', (request, response) => {

    const statement = ` select  c.custFirstName,c.custLastName,c.custEmail,b.custBookedHotel,b.custCheckInDate,
                       b.custCheckOutDate,b.custNoOfGuests,
                       b.custBookingStatus,b.custHotelRating,b.custNoOfRooms,b.cityPin
                       from custHotelBookingInfo b inner join customerInfo c
                       ON c.customerId=b.customerId`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
const mysql = require('mysql2')

// Connection to DataBase
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'TREB',
    password: 'manager',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})


module.exports = pool
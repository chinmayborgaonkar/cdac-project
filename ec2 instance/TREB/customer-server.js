const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
// Morgan: for logging
const morgan = require('morgan')

// Routers
const customerRouter = require('./customer/routes/customer')
const custGuestRouter = require('./customer/routes/cust_guest')
const custRestRouter = require('./customer/routes/cust_restaurant')
const custRestBookingRouter = require('./customer/routes/cust_Rest_booking')
const custHotelRouter = require('./customer/routes/cust_hotel')
const custBookingRouter = require('./customer/routes/cust_hotel_booking')
const cors = require('cors')

const app = express()
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('combined'))
app.use(cors('*'))

// Add a middleware for getting the id from token
function getCustomer(request, response, next) {

    if (request.url == '/customer/signin' ||
        request.url == '/customer/signup' ||
        request.url.startsWith('/customer/activate') ||
        request.url.startsWith('/customer/forgot-password') ||
        request.url.startsWith('/customer/image/') ||
        request.url.startsWith('/customer/restaurant/Id/') ||
        request.url == '/customer/restaurant/city' ||
        request.url == '/customer/restaurant/name' ||
        request.url == '/customer/restaurant/menuItemsName' ||
        request.url == ('/customer/hotel/city') ||
        request.url == ('/customer/hotel/price') ||
        request.url == ('/customer/hotel/name') ||
        request.url == ('/customer/hotel/Id')
    ) {
        next()
    } else {
        try {
            const token = request.headers['token']
            const data = jwt.verify(token, config.secret)
            request.userId = data['id']
            next()
        } catch (ex) {
            response.send({ status: 'error', error: 'protected api, access denied' })
        }
    }
}

app.use(getCustomer)

// Required to send the static files in the directory named images
app.use(express.static('images/'))

// Customer Routes
app.use('/customer', customerRouter)
app.use('/customer/guest', custGuestRouter)
app.use('/customer/restaurant', custRestRouter)
app.use('/customer/restaurant/booking', custRestBookingRouter)
app.use('/customer/hotel', custHotelRouter)
app.use('/customer/hotel/booking', custBookingRouter)

// Default Route
app.get('/', (request, response) => {
    response.send('welcome to TREB application')
})

// Customer Server on Port 3000
app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})
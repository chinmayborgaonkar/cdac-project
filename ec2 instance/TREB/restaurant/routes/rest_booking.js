const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

//========================= GET =========================

//========================= GET Bookings by status =========================
router.get('/status', (request, response) => {
    const { bookingStatus } = request.body

    let statement = '';

    if (bookingStatus == "Completed" || bookingStatus == "Cancelled") {
        statement = `select * from custRestBookingInfo where custBookingStatus='${bookingStatus}' AND restaurantId = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    } else {
        statement = `select * from custRestBookingInfo
                      where (custBookingStatus="Completed") 
                      AND ( CURDATE() = custRestBookingDate ) AND restaurantId  = ${request.userId}`

        db.query(statement, (error, data) => {
            const result = {
                "data": data,
                "booking Status": bookingStatus
            }
            response.send(utils.createResult(error, result));
        })
    }
})

//========================= Get Details of customer by hotelBookingId =========================
router.get('/custBookingDetails/:restBookingId', (request, response) => {

    const { restBookingId } = request.params

    const statement = `select c.custFirstName, c.custLastName, c.custEmail, c.custMobileNo, c.custAddress, c.cityPin
                        from customerInfo c inner join custRestBookingInfo h
                        on c.customerId = h.customerId
            where h.restBookingId = ${restBookingId} `


    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})
//========================= Get All Guests for Specific Customer =========================
router.get('/display-guest-list/:restBookingId', (request, response) => {
    const { restBookingId } = request.params

    const statement = `SELECT * FROM custRestGuestsList where restBookingId = ${restBookingId}`



    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Bookings by booking Id =========================
router.get('/byid', (request, response) => {
    const { restBookingId } = request.body

    const statement = ` select c.customerId, c.custFirstName, c.custLastName, r.restBookingId, r.custBookedRest, r.custRestBookingDate, r.custNoOfTables, r.custBookingStatus
                        from customerInfo c inner join custRestBookingInfo r
                        on c.customerId = r.customerId
                        where r.restBookingId = ${restBookingId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET all Bookings =========================
router.get('/all', (request, response) => {

    const statement = `select * from custRestBookingInfo WHERE restaurantId = ${request.userId};`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Search Booking by Booking ID =========================
router.get('/search-by-id/:restId', (request, response) => {
    const { restId } = request.params

    const statement = `select * from custRestBookingInfo WHERE  restaurantId = ${restId};`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Bookings by month and year =========================
router.get('/bymonthyear', (request, response) => {
    const { custRestBookingMonth, custRestBookingYear } = request.body

    const statement = ` select * from custRestBookingInfo where
                        YEAR(custRestBookingDate) = ${custRestBookingYear}
                        and MONTH(custRestBookingDate) = ${custRestBookingMonth}
                        and restaurantId = ${request.userId} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Bookings by booking / check-In Date =========================
router.get('/bydate', (request, response) => {
    const { custRestBookingDate } = request.body

    const statement = `select * from custRestBookingInfo where custRestBookingDate = '${custRestBookingDate}' and restaurantId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Bookings in a range of dates =========================
router.get('/byrange', (request, response) => {
    const { fromDate, toDate } = request.body

    const statement = `select * from custRestBookingInfo where custRestBookingDate BETWEEN '${fromDate}' and '${toDate}' and restaurantId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= GET Ratings for specific Restaurant =========================
router.get('/ratings', (request, response) => {


    const statement = `select r.restaurantId, r.custRestRating, c.customerId , c.custFirstName  , c.custLastName  
                        from custRestBookingInfo r inner join customerInfo c 
                        on c.customerId = r.customerId
                        where r.restaurantId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get booking by Customer name =========================
router.get('/byname', (request, response) => {
    const { custFirstName, custLastName } = request.body

    const statement = ` select c.custFirstName, c.custLastName, r.restBookingId, r.restaurantId, r.custBookedRest, r.custRestBookingDate, r.custRestRating, r.custBookingStatus, r.custNoOfTables
                        from customerInfo c inner join custRestBookingInfo r
                        on c.customerId = r.customerId
                        where c.custFirstName = '${custFirstName}'
                        and c.custLastName = '${custLastName}'
                        and r.restaurantId = ${request.userId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Get Guests lists for specific customer=========================
router.get('/getguests', (request, response) => {
    const { restBookingId } = request.body

    // getting guests by restBookingId
    const statement = `select g.custRestGuestsName, g.custRestGuestsAge, g.custRestGuestsGender, r.customerId, r.restBookingId
                        from custRestBookingInfo r
                        inner join custRestGuestsList g
                        on g.restBookingId = r.restBookingId
                        where r.restBookingId = ${restBookingId}`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Adding bookings =========================
router.post('/addbookings', (request, response) => {
    const { customerId, custBookedRest, custRestRating, custBookingStatus, custNoOfTables, custRestBookingDate, cityPin } = request.body

    const statement = `insert into custRestBookingInfo(customerId, restaurantId, custBookedRest, custRestRating, custBookingStatus, custNoOfTables, custRestBookingDate, cityPin)
                        values('${customerId}', '${request.userId}', '${custBookedRest}', '${custRestRating}', '${custBookingStatus}', '${custNoOfTables}', '${custRestBookingDate}', '${cityPin}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= Adding customer's Guests for restaurant =========================
router.post('/', (request, response) => {
    const { restBookingId, custRestGuestsName, custRestGuestsAge, custRestGuestsGender } = request.body

    const statement = `insert into custRestGuestsList(restBookingId, custRestGuestsName, custRestGuestsAge, custRestGuestsGender)
                        values ('${restBookingId}', '${custRestGuestsName}', '${custRestGuestsAge}', '${custRestGuestsGender}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router
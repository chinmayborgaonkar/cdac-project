const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')
const crypto = require('crypto-js')
const { response } = require('express')

const router = express.Router()


//========================= GET =========================

//========================= Get Table Details =========================
router.get("/", (request, response) => {
    const restId = request.userId
    const statement = `select restaurantId,restNoOfTableReserved ,restTotalNoOfTables from restTableDetails where restaurantId = '${restId}' `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= POST =========================

//========================= Add Table Details =========================
router.post('/', (request, response) => {
    const { noOfTables } = request.body
    const restId = request.userId

    const statement = `insert into restTableDetails (restaurantId , restTotalNoOfTables ) values (${restId}, ${noOfTables})`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

//========================= PUT =========================
//========================= Change Number Of Tables =========================
router.put('/', (request, response) => {
    const { noOfTables } = request.body
    const id = request.userId

    const statement1 = `update restTableDetails set restTotalNoOfTables = '${noOfTables}'  where restaurantId = '${id}'`

    db.query(statement1, (error, data) => {
        const statement2 = `update restaurantInfo set restNoOfTables = '${noOfTables}'  where restaurantId = '${id}'`
        db.query(statement2, (error, data1) => {
            response.send(utils.createResult(error, data1))
        })
    })
})

//========================= Update Table Details =========================
router.put('/reserve-table', (request, response) => {
    const { noOfTables, noOfReservedTables } = request.body
    const restId = request.userId

    const statement = `update restTableDetails set restNoOfTableReserved = '${noOfReservedTables}' where restaurantId = '${restId}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, {
            status: 'details added successfully',
            noOfAvailableTables: `${noOfTables - noOfReservedTables}`
        }))
    })
})

module.exports = router
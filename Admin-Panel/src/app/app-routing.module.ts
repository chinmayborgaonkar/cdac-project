import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';
import { RestaurantBookingComponent } from './restaurant-booking/restaurant-booking.component';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth/auth.service';

const routes: Routes = [
  {
    path:'',
    component:DashboardComponent,
    canActivate:[AuthService],
    children:[
      {path:'customer-list',component:CustomerListComponent},
      {path:'hotel-booking',component:HotelBookingComponent},
      {path:'hotel-list',component:HotelListComponent},
      {path:'restaurant-booking',component:RestaurantBookingComponent},
      {path:'restaurant-list',component:RestaurantListComponent},
    ]
  },
  {path:'auth',loadChildren:()=> import('./auth/auth.module').then(m=>m.AuthModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

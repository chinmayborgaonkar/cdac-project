import { Router, Routes } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email=''
  password=''
  constructor(
    private router:Router,
    private toastr:ToastrService
  ) { }

  ngOnInit(): void {
  }

  onLogin(){
    if(this.email=="takerest.eatbest@gmail.com" && this.password=="TREB")
    {
      sessionStorage['email']=this.email
      sessionStorage['password']=this.password
      this.router.navigate([''])
      this.toastr.success('welcome admin')
      return true 
    }
    else
    {
      this.toastr.error('invalid admin')
    }

  }
}

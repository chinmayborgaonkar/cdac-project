import { HotelService } from './hotel.service';
import { CustomerService } from './customer.service';
import { RestaurantService } from './restaurant.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { RestaurantBookingComponent } from './restaurant-booking/restaurant-booking.component';
import { HotelBookingComponent } from './hotel-booking/hotel-booking.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HotelListComponent,
    RestaurantListComponent,
    CustomerListComponent,
    RestaurantBookingComponent,
    HotelBookingComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [
    CustomerService,
    HotelService,
    RestaurantService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  url = 'http://localhost:4000/admin'

  constructor(
    private httpClient: HttpClient) { }
  
    getAllRestaurant()
    {
    return this.httpClient.get(this.url+"/get-rest")
    }

    getAllBookedRestaurant()
    {
    return this.httpClient.get(this.url+"/get-rest-booking")
    }
}

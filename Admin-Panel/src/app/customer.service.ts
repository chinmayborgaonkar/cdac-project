import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  url = 'http://localhost:4000/admin'

  constructor(
    private httpClient: HttpClient) { }
  
    getAllCustomers()
    {
    return this.httpClient.get(this.url+"/get-customer")
    }
}

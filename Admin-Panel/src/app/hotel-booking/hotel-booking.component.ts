import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-hotel-booking',
  templateUrl: './hotel-booking.component.html',
  styleUrls: ['./hotel-booking.component.css']
})
export class HotelBookingComponent implements OnInit {

  hotelbookings=[]
  
  constructor(
    private hotelService:HotelService
  ) { }

  ngOnInit(): void {
    this.loadBookedHotelsData()
  }

  loadBookedHotelsData(){
    this.hotelService.getAllBookedHotel().subscribe(response=>{
      if(response['status']=='success'){
        this.hotelbookings=response['data']
      }
    })
  }
}

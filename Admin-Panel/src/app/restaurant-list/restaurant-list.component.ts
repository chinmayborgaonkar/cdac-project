import { RestaurantService } from './../restaurant.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {

  restaurants = []

  constructor(
    private restaurantService: RestaurantService
  ) { }

  ngOnInit(): void {
    this.loadRestData()
  }

  loadRestData() {
    this.restaurantService.getAllRestaurant().subscribe(response => {
      if (response['status'] == 'success') {
        this.restaurants = response['data']

      }
    })
  }
}

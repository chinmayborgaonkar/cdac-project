import { RestaurantService } from './../restaurant.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurant-booking',
  templateUrl: './restaurant-booking.component.html',
  styleUrls: ['./restaurant-booking.component.css']
})
export class RestaurantBookingComponent implements OnInit {
  restbookings=[]
  
  constructor(
    private restaurantService: RestaurantService
  ) { }

  ngOnInit(): void {
    this.loadBookedRestsData()
  }

  loadBookedRestsData(){
    this.restaurantService.getAllBookedRestaurant().subscribe(response=>{
      if(response['status']=='success'){
        this.restbookings=response['data']
      }
    })
  }
}

import { CustomerService } from './../customer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  customers = []

  constructor(
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    this.loadCustomersData()
  }

  loadCustomersData() {
    this.customerService.getAllCustomers().subscribe(response => {
      if (response['status'] == 'success') {
        this.customers = response['data']
        console.log(this.customers)
      }
    })
  }

}

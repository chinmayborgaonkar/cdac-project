import { HotelService } from './../hotel.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {

  hotels = []

  constructor(
    private hotelService: HotelService
  ) { }

  ngOnInit(): void {
    this.loadHotelsData()
  }

  loadHotelsData() {
    this.hotelService.getAllHotels().subscribe(response => {
      if (response['status'] == 'success') {
        this.hotels = response['data']

      }
    })
  }
}

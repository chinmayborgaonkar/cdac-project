import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  url = 'http://localhost:4000/admin'

  constructor(
    private httpClient: HttpClient) { }
  
    getAllHotels()
    {
    return this.httpClient.get(this.url+"/get-hotel")
    }

    
    getAllBookedHotel()
    {
    return this.httpClient.get(this.url+"/get-hotel-booking")
    }
}
